# Generated from Niklaus.g by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .NiklausParser import NiklausParser
else:
    from NiklausParser import NiklausParser

# This class defines a complete listener for a parse tree produced by NiklausParser.
class NiklausListener(ParseTreeListener):

    # Enter a parse tree produced by NiklausParser#prog.
    def enterProg(self, ctx:NiklausParser.ProgContext):
        pass

    # Exit a parse tree produced by NiklausParser#prog.
    def exitProg(self, ctx:NiklausParser.ProgContext):
        pass


    # Enter a parse tree produced by NiklausParser#nom_prog.
    def enterNom_prog(self, ctx:NiklausParser.Nom_progContext):
        pass

    # Exit a parse tree produced by NiklausParser#nom_prog.
    def exitNom_prog(self, ctx:NiklausParser.Nom_progContext):
        pass


    # Enter a parse tree produced by NiklausParser#decl_var.
    def enterDecl_var(self, ctx:NiklausParser.Decl_varContext):
        pass

    # Exit a parse tree produced by NiklausParser#decl_var.
    def exitDecl_var(self, ctx:NiklausParser.Decl_varContext):
        pass


    # Enter a parse tree produced by NiklausParser#bloc.
    def enterBloc(self, ctx:NiklausParser.BlocContext):
        pass

    # Exit a parse tree produced by NiklausParser#bloc.
    def exitBloc(self, ctx:NiklausParser.BlocContext):
        pass


    # Enter a parse tree produced by NiklausParser#InstrRead.
    def enterInstrRead(self, ctx:NiklausParser.InstrReadContext):
        pass

    # Exit a parse tree produced by NiklausParser#InstrRead.
    def exitInstrRead(self, ctx:NiklausParser.InstrReadContext):
        pass


    # Enter a parse tree produced by NiklausParser#InstrWrite.
    def enterInstrWrite(self, ctx:NiklausParser.InstrWriteContext):
        pass

    # Exit a parse tree produced by NiklausParser#InstrWrite.
    def exitInstrWrite(self, ctx:NiklausParser.InstrWriteContext):
        pass


    # Enter a parse tree produced by NiklausParser#InstrAffect.
    def enterInstrAffect(self, ctx:NiklausParser.InstrAffectContext):
        pass

    # Exit a parse tree produced by NiklausParser#InstrAffect.
    def exitInstrAffect(self, ctx:NiklausParser.InstrAffectContext):
        pass


    # Enter a parse tree produced by NiklausParser#InstrWhile.
    def enterInstrWhile(self, ctx:NiklausParser.InstrWhileContext):
        pass

    # Exit a parse tree produced by NiklausParser#InstrWhile.
    def exitInstrWhile(self, ctx:NiklausParser.InstrWhileContext):
        pass


    # Enter a parse tree produced by NiklausParser#InstrCond.
    def enterInstrCond(self, ctx:NiklausParser.InstrCondContext):
        pass

    # Exit a parse tree produced by NiklausParser#InstrCond.
    def exitInstrCond(self, ctx:NiklausParser.InstrCondContext):
        pass


    # Enter a parse tree produced by NiklausParser#read.
    def enterRead(self, ctx:NiklausParser.ReadContext):
        pass

    # Exit a parse tree produced by NiklausParser#read.
    def exitRead(self, ctx:NiklausParser.ReadContext):
        pass


    # Enter a parse tree produced by NiklausParser#write.
    def enterWrite(self, ctx:NiklausParser.WriteContext):
        pass

    # Exit a parse tree produced by NiklausParser#write.
    def exitWrite(self, ctx:NiklausParser.WriteContext):
        pass


    # Enter a parse tree produced by NiklausParser#affect.
    def enterAffect(self, ctx:NiklausParser.AffectContext):
        pass

    # Exit a parse tree produced by NiklausParser#affect.
    def exitAffect(self, ctx:NiklausParser.AffectContext):
        pass


    # Enter a parse tree produced by NiklausParser#b_while.
    def enterB_while(self, ctx:NiklausParser.B_whileContext):
        pass

    # Exit a parse tree produced by NiklausParser#b_while.
    def exitB_while(self, ctx:NiklausParser.B_whileContext):
        pass


    # Enter a parse tree produced by NiklausParser#cond.
    def enterCond(self, ctx:NiklausParser.CondContext):
        pass

    # Exit a parse tree produced by NiklausParser#cond.
    def exitCond(self, ctx:NiklausParser.CondContext):
        pass


    # Enter a parse tree produced by NiklausParser#expr.
    def enterExpr(self, ctx:NiklausParser.ExprContext):
        pass

    # Exit a parse tree produced by NiklausParser#expr.
    def exitExpr(self, ctx:NiklausParser.ExprContext):
        pass


    # Enter a parse tree produced by NiklausParser#term.
    def enterTerm(self, ctx:NiklausParser.TermContext):
        pass

    # Exit a parse tree produced by NiklausParser#term.
    def exitTerm(self, ctx:NiklausParser.TermContext):
        pass


    # Enter a parse tree produced by NiklausParser#comp.
    def enterComp(self, ctx:NiklausParser.CompContext):
        pass

    # Exit a parse tree produced by NiklausParser#comp.
    def exitComp(self, ctx:NiklausParser.CompContext):
        pass


    # Enter a parse tree produced by NiklausParser#FactorId.
    def enterFactorId(self, ctx:NiklausParser.FactorIdContext):
        pass

    # Exit a parse tree produced by NiklausParser#FactorId.
    def exitFactorId(self, ctx:NiklausParser.FactorIdContext):
        pass


    # Enter a parse tree produced by NiklausParser#FactorInt.
    def enterFactorInt(self, ctx:NiklausParser.FactorIntContext):
        pass

    # Exit a parse tree produced by NiklausParser#FactorInt.
    def exitFactorInt(self, ctx:NiklausParser.FactorIntContext):
        pass


    # Enter a parse tree produced by NiklausParser#FactorBlock.
    def enterFactorBlock(self, ctx:NiklausParser.FactorBlockContext):
        pass

    # Exit a parse tree produced by NiklausParser#FactorBlock.
    def exitFactorBlock(self, ctx:NiklausParser.FactorBlockContext):
        pass


