# Generated from Niklaus.g by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .NiklausParser import NiklausParser
else:
    from NiklausParser import NiklausParser

# This class defines a complete generic visitor for a parse tree produced by NiklausParser.

class NiklausVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by NiklausParser#prog.
    def visitProg(self, ctx:NiklausParser.ProgContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#nom_prog.
    def visitNom_prog(self, ctx:NiklausParser.Nom_progContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#decl_var.
    def visitDecl_var(self, ctx:NiklausParser.Decl_varContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#bloc.
    def visitBloc(self, ctx:NiklausParser.BlocContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#InstrRead.
    def visitInstrRead(self, ctx:NiklausParser.InstrReadContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#InstrWrite.
    def visitInstrWrite(self, ctx:NiklausParser.InstrWriteContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#InstrAffect.
    def visitInstrAffect(self, ctx:NiklausParser.InstrAffectContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#InstrWhile.
    def visitInstrWhile(self, ctx:NiklausParser.InstrWhileContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#InstrCond.
    def visitInstrCond(self, ctx:NiklausParser.InstrCondContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#read.
    def visitRead(self, ctx:NiklausParser.ReadContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#write.
    def visitWrite(self, ctx:NiklausParser.WriteContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#affect.
    def visitAffect(self, ctx:NiklausParser.AffectContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#b_while.
    def visitB_while(self, ctx:NiklausParser.B_whileContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#cond.
    def visitCond(self, ctx:NiklausParser.CondContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#expr.
    def visitExpr(self, ctx:NiklausParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#term.
    def visitTerm(self, ctx:NiklausParser.TermContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#comp.
    def visitComp(self, ctx:NiklausParser.CompContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#FactorId.
    def visitFactorId(self, ctx:NiklausParser.FactorIdContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#FactorInt.
    def visitFactorInt(self, ctx:NiklausParser.FactorIntContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by NiklausParser#FactorBlock.
    def visitFactorBlock(self, ctx:NiklausParser.FactorBlockContext):
        return self.visitChildren(ctx)



del NiklausParser