# Generated from Niklaus.g by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\27")
        buf.write("\u0080\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\3\2\3\2\5\2!\n\2\3\2\3\2\3\2\3\2\3\2\3")
        buf.write("\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\7\4\60\n\4\f\4\16\4\63")
        buf.write("\13\4\3\4\3\4\3\5\6\58\n\5\r\5\16\59\3\6\3\6\3\6\3\6\3")
        buf.write("\6\5\6A\n\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3")
        buf.write("\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3")
        buf.write("\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3")
        buf.write("\f\3\f\7\fg\n\f\f\f\16\fj\13\f\3\r\3\r\3\r\7\ro\n\r\f")
        buf.write("\r\16\rr\13\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\5\17~\n\17\3\17\2\2\20\2\4\6\b\n\f\16\20\22")
        buf.write("\24\26\30\32\34\2\2\2|\2\36\3\2\2\2\4\'\3\2\2\2\6+\3\2")
        buf.write("\2\2\b\67\3\2\2\2\n@\3\2\2\2\fB\3\2\2\2\16F\3\2\2\2\20")
        buf.write("J\3\2\2\2\22O\3\2\2\2\24W\3\2\2\2\26c\3\2\2\2\30k\3\2")
        buf.write("\2\2\32s\3\2\2\2\34}\3\2\2\2\36 \5\4\3\2\37!\5\6\4\2 ")
        buf.write("\37\3\2\2\2 !\3\2\2\2!\"\3\2\2\2\"#\7\3\2\2#$\5\b\5\2")
        buf.write("$%\7\4\2\2%&\7\2\2\3&\3\3\2\2\2\'(\7\5\2\2()\7\27\2\2")
        buf.write(")*\7\6\2\2*\5\3\2\2\2+,\7\7\2\2,\61\7\27\2\2-.\7\b\2\2")
        buf.write(".\60\7\27\2\2/-\3\2\2\2\60\63\3\2\2\2\61/\3\2\2\2\61\62")
        buf.write("\3\2\2\2\62\64\3\2\2\2\63\61\3\2\2\2\64\65\7\6\2\2\65")
        buf.write("\7\3\2\2\2\668\5\n\6\2\67\66\3\2\2\289\3\2\2\29\67\3\2")
        buf.write("\2\29:\3\2\2\2:\t\3\2\2\2;A\5\f\7\2<A\5\16\b\2=A\5\20")
        buf.write("\t\2>A\5\22\n\2?A\5\24\13\2@;\3\2\2\2@<\3\2\2\2@=\3\2")
        buf.write("\2\2@>\3\2\2\2@?\3\2\2\2A\13\3\2\2\2BC\7\t\2\2CD\7\27")
        buf.write("\2\2DE\7\6\2\2E\r\3\2\2\2FG\7\n\2\2GH\5\26\f\2HI\7\6\2")
        buf.write("\2I\17\3\2\2\2JK\7\27\2\2KL\7\13\2\2LM\5\26\f\2MN\7\6")
        buf.write("\2\2N\21\3\2\2\2OP\7\f\2\2PQ\7\r\2\2QR\5\32\16\2RS\7\16")
        buf.write("\2\2ST\7\3\2\2TU\5\b\5\2UV\7\4\2\2V\23\3\2\2\2WX\7\17")
        buf.write("\2\2XY\7\r\2\2YZ\5\32\16\2Z[\7\16\2\2[\\\7\3\2\2\\]\5")
        buf.write("\b\5\2]^\7\4\2\2^_\7\20\2\2_`\7\3\2\2`a\5\b\5\2ab\7\4")
        buf.write("\2\2b\25\3\2\2\2ch\5\30\r\2de\7\25\2\2eg\5\30\r\2fd\3")
        buf.write("\2\2\2gj\3\2\2\2hf\3\2\2\2hi\3\2\2\2i\27\3\2\2\2jh\3\2")
        buf.write("\2\2kp\5\34\17\2lm\7\26\2\2mo\5\34\17\2nl\3\2\2\2or\3")
        buf.write("\2\2\2pn\3\2\2\2pq\3\2\2\2q\31\3\2\2\2rp\3\2\2\2st\5\26")
        buf.write("\f\2tu\7\21\2\2uv\5\26\f\2v\33\3\2\2\2w~\7\27\2\2x~\7")
        buf.write("\22\2\2yz\7\r\2\2z{\5\26\f\2{|\7\16\2\2|~\3\2\2\2}w\3")
        buf.write("\2\2\2}x\3\2\2\2}y\3\2\2\2~\35\3\2\2\2\t \619@hp}")
        return buf.getvalue()


class NiklausParser ( Parser ):

    grammarFileName = "Niklaus.g"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'{'", "'}'", "'program'", "';'", "'var'", 
                     "','", "'read'", "'write'", "':='", "'while'", "'('", 
                     "')'", "'if'", "'else'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "COMPARATOR", 
                      "INT", "COMMENT", "WS", "ADDOP", "MULTOP", "ID" ]

    RULE_prog = 0
    RULE_nom_prog = 1
    RULE_decl_var = 2
    RULE_bloc = 3
    RULE_instr = 4
    RULE_read = 5
    RULE_write = 6
    RULE_affect = 7
    RULE_b_while = 8
    RULE_cond = 9
    RULE_expr = 10
    RULE_term = 11
    RULE_comp = 12
    RULE_factor = 13

    ruleNames =  [ "prog", "nom_prog", "decl_var", "bloc", "instr", "read", 
                   "write", "affect", "b_while", "cond", "expr", "term", 
                   "comp", "factor" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    COMPARATOR=15
    INT=16
    COMMENT=17
    WS=18
    ADDOP=19
    MULTOP=20
    ID=21

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def nom_prog(self):
            return self.getTypedRuleContext(NiklausParser.Nom_progContext,0)


        def bloc(self):
            return self.getTypedRuleContext(NiklausParser.BlocContext,0)


        def EOF(self):
            return self.getToken(NiklausParser.EOF, 0)

        def decl_var(self):
            return self.getTypedRuleContext(NiklausParser.Decl_varContext,0)


        def getRuleIndex(self):
            return NiklausParser.RULE_prog

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProg" ):
                listener.enterProg(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProg" ):
                listener.exitProg(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProg" ):
                return visitor.visitProg(self)
            else:
                return visitor.visitChildren(self)




    def prog(self):

        localctx = NiklausParser.ProgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_prog)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 28
            self.nom_prog()
            self.state = 30
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==NiklausParser.T__4:
                self.state = 29
                self.decl_var()


            self.state = 32
            self.match(NiklausParser.T__0)
            self.state = 33
            self.bloc()
            self.state = 34
            self.match(NiklausParser.T__1)
            self.state = 35
            self.match(NiklausParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Nom_progContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(NiklausParser.ID, 0)

        def getRuleIndex(self):
            return NiklausParser.RULE_nom_prog

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNom_prog" ):
                listener.enterNom_prog(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNom_prog" ):
                listener.exitNom_prog(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNom_prog" ):
                return visitor.visitNom_prog(self)
            else:
                return visitor.visitChildren(self)




    def nom_prog(self):

        localctx = NiklausParser.Nom_progContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_nom_prog)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 37
            self.match(NiklausParser.T__2)
            self.state = 38
            self.match(NiklausParser.ID)
            self.state = 39
            self.match(NiklausParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Decl_varContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(NiklausParser.ID)
            else:
                return self.getToken(NiklausParser.ID, i)

        def getRuleIndex(self):
            return NiklausParser.RULE_decl_var

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDecl_var" ):
                listener.enterDecl_var(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDecl_var" ):
                listener.exitDecl_var(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDecl_var" ):
                return visitor.visitDecl_var(self)
            else:
                return visitor.visitChildren(self)




    def decl_var(self):

        localctx = NiklausParser.Decl_varContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_decl_var)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 41
            self.match(NiklausParser.T__4)
            self.state = 42
            self.match(NiklausParser.ID)
            self.state = 47
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==NiklausParser.T__5:
                self.state = 43
                self.match(NiklausParser.T__5)
                self.state = 44
                self.match(NiklausParser.ID)
                self.state = 49
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 50
            self.match(NiklausParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BlocContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def instr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(NiklausParser.InstrContext)
            else:
                return self.getTypedRuleContext(NiklausParser.InstrContext,i)


        def getRuleIndex(self):
            return NiklausParser.RULE_bloc

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBloc" ):
                listener.enterBloc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBloc" ):
                listener.exitBloc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBloc" ):
                return visitor.visitBloc(self)
            else:
                return visitor.visitChildren(self)




    def bloc(self):

        localctx = NiklausParser.BlocContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_bloc)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 53 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 52
                self.instr()
                self.state = 55 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << NiklausParser.T__6) | (1 << NiklausParser.T__7) | (1 << NiklausParser.T__9) | (1 << NiklausParser.T__12) | (1 << NiklausParser.ID))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class InstrContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return NiklausParser.RULE_instr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class InstrReadContext(InstrContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a NiklausParser.InstrContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def read(self):
            return self.getTypedRuleContext(NiklausParser.ReadContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInstrRead" ):
                listener.enterInstrRead(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInstrRead" ):
                listener.exitInstrRead(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInstrRead" ):
                return visitor.visitInstrRead(self)
            else:
                return visitor.visitChildren(self)


    class InstrWhileContext(InstrContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a NiklausParser.InstrContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def b_while(self):
            return self.getTypedRuleContext(NiklausParser.B_whileContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInstrWhile" ):
                listener.enterInstrWhile(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInstrWhile" ):
                listener.exitInstrWhile(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInstrWhile" ):
                return visitor.visitInstrWhile(self)
            else:
                return visitor.visitChildren(self)


    class InstrCondContext(InstrContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a NiklausParser.InstrContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def cond(self):
            return self.getTypedRuleContext(NiklausParser.CondContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInstrCond" ):
                listener.enterInstrCond(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInstrCond" ):
                listener.exitInstrCond(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInstrCond" ):
                return visitor.visitInstrCond(self)
            else:
                return visitor.visitChildren(self)


    class InstrWriteContext(InstrContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a NiklausParser.InstrContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def write(self):
            return self.getTypedRuleContext(NiklausParser.WriteContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInstrWrite" ):
                listener.enterInstrWrite(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInstrWrite" ):
                listener.exitInstrWrite(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInstrWrite" ):
                return visitor.visitInstrWrite(self)
            else:
                return visitor.visitChildren(self)


    class InstrAffectContext(InstrContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a NiklausParser.InstrContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def affect(self):
            return self.getTypedRuleContext(NiklausParser.AffectContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInstrAffect" ):
                listener.enterInstrAffect(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInstrAffect" ):
                listener.exitInstrAffect(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInstrAffect" ):
                return visitor.visitInstrAffect(self)
            else:
                return visitor.visitChildren(self)



    def instr(self):

        localctx = NiklausParser.InstrContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_instr)
        try:
            self.state = 62
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [NiklausParser.T__6]:
                localctx = NiklausParser.InstrReadContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 57
                self.read()
                pass
            elif token in [NiklausParser.T__7]:
                localctx = NiklausParser.InstrWriteContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 58
                self.write()
                pass
            elif token in [NiklausParser.ID]:
                localctx = NiklausParser.InstrAffectContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 59
                self.affect()
                pass
            elif token in [NiklausParser.T__9]:
                localctx = NiklausParser.InstrWhileContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 60
                self.b_while()
                pass
            elif token in [NiklausParser.T__12]:
                localctx = NiklausParser.InstrCondContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 61
                self.cond()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ReadContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(NiklausParser.ID, 0)

        def getRuleIndex(self):
            return NiklausParser.RULE_read

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRead" ):
                listener.enterRead(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRead" ):
                listener.exitRead(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRead" ):
                return visitor.visitRead(self)
            else:
                return visitor.visitChildren(self)




    def read(self):

        localctx = NiklausParser.ReadContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_read)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 64
            self.match(NiklausParser.T__6)
            self.state = 65
            self.match(NiklausParser.ID)
            self.state = 66
            self.match(NiklausParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class WriteContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(NiklausParser.ExprContext,0)


        def getRuleIndex(self):
            return NiklausParser.RULE_write

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWrite" ):
                listener.enterWrite(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWrite" ):
                listener.exitWrite(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWrite" ):
                return visitor.visitWrite(self)
            else:
                return visitor.visitChildren(self)




    def write(self):

        localctx = NiklausParser.WriteContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_write)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 68
            self.match(NiklausParser.T__7)
            self.state = 69
            self.expr()
            self.state = 70
            self.match(NiklausParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AffectContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(NiklausParser.ID, 0)

        def expr(self):
            return self.getTypedRuleContext(NiklausParser.ExprContext,0)


        def getRuleIndex(self):
            return NiklausParser.RULE_affect

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAffect" ):
                listener.enterAffect(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAffect" ):
                listener.exitAffect(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAffect" ):
                return visitor.visitAffect(self)
            else:
                return visitor.visitChildren(self)




    def affect(self):

        localctx = NiklausParser.AffectContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_affect)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 72
            self.match(NiklausParser.ID)
            self.state = 73
            self.match(NiklausParser.T__8)
            self.state = 74
            self.expr()
            self.state = 75
            self.match(NiklausParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class B_whileContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def comp(self):
            return self.getTypedRuleContext(NiklausParser.CompContext,0)


        def bloc(self):
            return self.getTypedRuleContext(NiklausParser.BlocContext,0)


        def getRuleIndex(self):
            return NiklausParser.RULE_b_while

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterB_while" ):
                listener.enterB_while(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitB_while" ):
                listener.exitB_while(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitB_while" ):
                return visitor.visitB_while(self)
            else:
                return visitor.visitChildren(self)




    def b_while(self):

        localctx = NiklausParser.B_whileContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_b_while)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 77
            self.match(NiklausParser.T__9)
            self.state = 78
            self.match(NiklausParser.T__10)
            self.state = 79
            self.comp()
            self.state = 80
            self.match(NiklausParser.T__11)
            self.state = 81
            self.match(NiklausParser.T__0)
            self.state = 82
            self.bloc()
            self.state = 83
            self.match(NiklausParser.T__1)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class CondContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def comp(self):
            return self.getTypedRuleContext(NiklausParser.CompContext,0)


        def bloc(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(NiklausParser.BlocContext)
            else:
                return self.getTypedRuleContext(NiklausParser.BlocContext,i)


        def getRuleIndex(self):
            return NiklausParser.RULE_cond

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCond" ):
                listener.enterCond(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCond" ):
                listener.exitCond(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCond" ):
                return visitor.visitCond(self)
            else:
                return visitor.visitChildren(self)




    def cond(self):

        localctx = NiklausParser.CondContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_cond)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            self.match(NiklausParser.T__12)
            self.state = 86
            self.match(NiklausParser.T__10)
            self.state = 87
            self.comp()
            self.state = 88
            self.match(NiklausParser.T__11)
            self.state = 89
            self.match(NiklausParser.T__0)
            self.state = 90
            self.bloc()
            self.state = 91
            self.match(NiklausParser.T__1)
            self.state = 92
            self.match(NiklausParser.T__13)
            self.state = 93
            self.match(NiklausParser.T__0)
            self.state = 94
            self.bloc()
            self.state = 95
            self.match(NiklausParser.T__1)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(NiklausParser.TermContext)
            else:
                return self.getTypedRuleContext(NiklausParser.TermContext,i)


        def ADDOP(self, i:int=None):
            if i is None:
                return self.getTokens(NiklausParser.ADDOP)
            else:
                return self.getToken(NiklausParser.ADDOP, i)

        def getRuleIndex(self):
            return NiklausParser.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = NiklausParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 97
            self.term()
            self.state = 102
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==NiklausParser.ADDOP:
                self.state = 98
                self.match(NiklausParser.ADDOP)
                self.state = 99
                self.term()
                self.state = 104
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def factor(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(NiklausParser.FactorContext)
            else:
                return self.getTypedRuleContext(NiklausParser.FactorContext,i)


        def MULTOP(self, i:int=None):
            if i is None:
                return self.getTokens(NiklausParser.MULTOP)
            else:
                return self.getToken(NiklausParser.MULTOP, i)

        def getRuleIndex(self):
            return NiklausParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerm" ):
                return visitor.visitTerm(self)
            else:
                return visitor.visitChildren(self)




    def term(self):

        localctx = NiklausParser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_term)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 105
            self.factor()
            self.state = 110
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==NiklausParser.MULTOP:
                self.state = 106
                self.match(NiklausParser.MULTOP)
                self.state = 107
                self.factor()
                self.state = 112
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class CompContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(NiklausParser.ExprContext)
            else:
                return self.getTypedRuleContext(NiklausParser.ExprContext,i)


        def COMPARATOR(self):
            return self.getToken(NiklausParser.COMPARATOR, 0)

        def getRuleIndex(self):
            return NiklausParser.RULE_comp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComp" ):
                listener.enterComp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComp" ):
                listener.exitComp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComp" ):
                return visitor.visitComp(self)
            else:
                return visitor.visitChildren(self)




    def comp(self):

        localctx = NiklausParser.CompContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_comp)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 113
            self.expr()
            self.state = 114
            self.match(NiklausParser.COMPARATOR)
            self.state = 115
            self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FactorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return NiklausParser.RULE_factor

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class FactorBlockContext(FactorContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a NiklausParser.FactorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(NiklausParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactorBlock" ):
                listener.enterFactorBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactorBlock" ):
                listener.exitFactorBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFactorBlock" ):
                return visitor.visitFactorBlock(self)
            else:
                return visitor.visitChildren(self)


    class FactorIntContext(FactorContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a NiklausParser.FactorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def INT(self):
            return self.getToken(NiklausParser.INT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactorInt" ):
                listener.enterFactorInt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactorInt" ):
                listener.exitFactorInt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFactorInt" ):
                return visitor.visitFactorInt(self)
            else:
                return visitor.visitChildren(self)


    class FactorIdContext(FactorContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a NiklausParser.FactorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(NiklausParser.ID, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactorId" ):
                listener.enterFactorId(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactorId" ):
                listener.exitFactorId(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFactorId" ):
                return visitor.visitFactorId(self)
            else:
                return visitor.visitChildren(self)



    def factor(self):

        localctx = NiklausParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_factor)
        try:
            self.state = 123
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [NiklausParser.ID]:
                localctx = NiklausParser.FactorIdContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 117
                self.match(NiklausParser.ID)
                pass
            elif token in [NiklausParser.INT]:
                localctx = NiklausParser.FactorIntContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 118
                self.match(NiklausParser.INT)
                pass
            elif token in [NiklausParser.T__10]:
                localctx = NiklausParser.FactorBlockContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 119
                self.match(NiklausParser.T__10)
                self.state = 120
                self.expr()
                self.state = 121
                self.match(NiklausParser.T__11)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





