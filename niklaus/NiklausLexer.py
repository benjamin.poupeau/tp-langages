# Generated from Niklaus.g by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\27")
        buf.write("\u0092\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\3\2\3\2\3\3\3\3\3")
        buf.write("\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\6\3\6\3\6\3\6")
        buf.write("\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3")
        buf.write("\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\r\3")
        buf.write("\r\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3")
        buf.write("\20\3\20\3\20\3\20\3\20\5\20i\n\20\3\21\6\21l\n\21\r\21")
        buf.write("\16\21m\3\22\3\22\3\22\3\22\7\22t\n\22\f\22\16\22w\13")
        buf.write("\22\3\22\5\22z\n\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23")
        buf.write("\3\23\3\24\3\24\3\24\3\24\5\24\u0088\n\24\3\25\3\25\3")
        buf.write("\26\3\26\7\26\u008e\n\26\f\26\16\26\u0091\13\26\2\2\27")
        buf.write("\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31")
        buf.write("\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27\3\2\t\3")
        buf.write("\2\62;\4\2\f\f\17\17\5\2\13\f\17\17\"\"\4\2--//\4\2,,")
        buf.write("\61\61\5\2C\\aac|\6\2\62;C\\aac|\2\u0099\2\3\3\2\2\2\2")
        buf.write("\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3")
        buf.write("\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2")
        buf.write("\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2")
        buf.write("\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3")
        buf.write("\2\2\2\2)\3\2\2\2\2+\3\2\2\2\3-\3\2\2\2\5/\3\2\2\2\7\61")
        buf.write("\3\2\2\2\t9\3\2\2\2\13;\3\2\2\2\r?\3\2\2\2\17A\3\2\2\2")
        buf.write("\21F\3\2\2\2\23L\3\2\2\2\25O\3\2\2\2\27U\3\2\2\2\31W\3")
        buf.write("\2\2\2\33Y\3\2\2\2\35\\\3\2\2\2\37h\3\2\2\2!k\3\2\2\2")
        buf.write("#o\3\2\2\2%\177\3\2\2\2\'\u0087\3\2\2\2)\u0089\3\2\2\2")
        buf.write("+\u008b\3\2\2\2-.\7}\2\2.\4\3\2\2\2/\60\7\177\2\2\60\6")
        buf.write("\3\2\2\2\61\62\7r\2\2\62\63\7t\2\2\63\64\7q\2\2\64\65")
        buf.write("\7i\2\2\65\66\7t\2\2\66\67\7c\2\2\678\7o\2\28\b\3\2\2")
        buf.write("\29:\7=\2\2:\n\3\2\2\2;<\7x\2\2<=\7c\2\2=>\7t\2\2>\f\3")
        buf.write("\2\2\2?@\7.\2\2@\16\3\2\2\2AB\7t\2\2BC\7g\2\2CD\7c\2\2")
        buf.write("DE\7f\2\2E\20\3\2\2\2FG\7y\2\2GH\7t\2\2HI\7k\2\2IJ\7v")
        buf.write("\2\2JK\7g\2\2K\22\3\2\2\2LM\7<\2\2MN\7?\2\2N\24\3\2\2")
        buf.write("\2OP\7y\2\2PQ\7j\2\2QR\7k\2\2RS\7n\2\2ST\7g\2\2T\26\3")
        buf.write("\2\2\2UV\7*\2\2V\30\3\2\2\2WX\7+\2\2X\32\3\2\2\2YZ\7k")
        buf.write("\2\2Z[\7h\2\2[\34\3\2\2\2\\]\7g\2\2]^\7n\2\2^_\7u\2\2")
        buf.write("_`\7g\2\2`\36\3\2\2\2ai\4>@\2bc\7>\2\2ci\7?\2\2de\7@\2")
        buf.write("\2ei\7?\2\2fg\7>\2\2gi\7@\2\2ha\3\2\2\2hb\3\2\2\2hd\3")
        buf.write("\2\2\2hf\3\2\2\2i \3\2\2\2jl\t\2\2\2kj\3\2\2\2lm\3\2\2")
        buf.write("\2mk\3\2\2\2mn\3\2\2\2n\"\3\2\2\2op\7\61\2\2pq\7\61\2")
        buf.write("\2qu\3\2\2\2rt\n\3\2\2sr\3\2\2\2tw\3\2\2\2us\3\2\2\2u")
        buf.write("v\3\2\2\2vy\3\2\2\2wu\3\2\2\2xz\7\17\2\2yx\3\2\2\2yz\3")
        buf.write("\2\2\2z{\3\2\2\2{|\7\f\2\2|}\3\2\2\2}~\b\22\2\2~$\3\2")
        buf.write("\2\2\177\u0080\t\4\2\2\u0080\u0081\3\2\2\2\u0081\u0082")
        buf.write("\b\23\2\2\u0082&\3\2\2\2\u0083\u0088\t\5\2\2\u0084\u0085")
        buf.write("\7o\2\2\u0085\u0086\7q\2\2\u0086\u0088\7f\2\2\u0087\u0083")
        buf.write("\3\2\2\2\u0087\u0084\3\2\2\2\u0088(\3\2\2\2\u0089\u008a")
        buf.write("\t\6\2\2\u008a*\3\2\2\2\u008b\u008f\t\7\2\2\u008c\u008e")
        buf.write("\t\b\2\2\u008d\u008c\3\2\2\2\u008e\u0091\3\2\2\2\u008f")
        buf.write("\u008d\3\2\2\2\u008f\u0090\3\2\2\2\u0090,\3\2\2\2\u0091")
        buf.write("\u008f\3\2\2\2\t\2hmuy\u0087\u008f\3\b\2\2")
        return buf.getvalue()


class NiklausLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    COMPARATOR = 15
    INT = 16
    COMMENT = 17
    WS = 18
    ADDOP = 19
    MULTOP = 20
    ID = 21

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'{'", "'}'", "'program'", "';'", "'var'", "','", "'read'", 
            "'write'", "':='", "'while'", "'('", "')'", "'if'", "'else'" ]

    symbolicNames = [ "<INVALID>",
            "COMPARATOR", "INT", "COMMENT", "WS", "ADDOP", "MULTOP", "ID" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "COMPARATOR", "INT", "COMMENT", "WS", "ADDOP", "MULTOP", 
                  "ID" ]

    grammarFileName = "Niklaus.g"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


