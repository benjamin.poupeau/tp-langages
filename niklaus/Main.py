import sys
from antlr4 import *
from NiklausLexer import NiklausLexer
from NiklausParser import NiklausParser
from VisitorInterp import VisitorInterp

def main(argv):
    input_stream = FileStream(argv[1])
    lexer = NiklausParser(input_stream)
    stream = CommonTokenStream(lexer)
    parser = NiklausParser(stream)
    tree = parser.prog()
    if parser.getNumberOfSyntaxErrors() > 0:
        print("syntax errors")
    else:
        vinterp = VisitorInterp()
        print(vinterp.visit(tree))

if __name__ == '__main__':
    main(sys.argv)