from antlr4 import *
from NiklausParser import NiklausParser
from NiklausVisitor import NiklausVisitor
"""

    A revoir : 
        - utilisation des self et des ctx 
        - read/write
        - utilisations de self.visit(ctx.getChild(i)) au lieu de self.getChild(i)

"""

class VisitorInterp (NiklausVisitor):

    dico_var = {} 
    @staticmethod
    def lire_fichier_texte(chemin_fichier):
        try:
            with open(chemin_fichier, 'r', encoding='utf-8') as fichier:
                contenu = fichier.read()
                return contenu
        except FileNotFoundError:
            print(f"Le fichier {chemin_fichier} n'a pas été trouvé.")
        except Exception as e:
            print(f"Une erreur s'est produite : {e}")



    def visitProg(self, ctx: NiklausParser.ProgContext):
        instrPile = "mov r6, adrrPile\n"
        if ctx.getChildCount() == 6:
            instrBloc = self.visit(ctx.getChild(4))
            instrDeclarVar = self.visit(ctx.getChild(1))
        else:
            instrBloc = self.visit(ctx.getChild(3))
            instrDeclarVar = ""
        instrFin = "@fin b fin\n"
        
        instrLib = self.lire_fichier_texte("lib.arm")
        instrDeclarPile = "@addrPile rmw 1\n"
        return instrPile  + instrBloc + instrFin +  instrDeclarVar + instrLib + instrDeclarPile
    

    def visitNom_prog(self, ctx: NiklausParser.Nom_progContext):
        return "% " +self.visit(ctx.getChild(1)) + "\n" #Nom du programme en commentaire

    def visitDecl_var(self, ctx: NiklausParser.Decl_varContext):
        """
            Objectif : Déclarer les variables par une série de rmw / smw
        """
        
        for i in range(ctx.getChildCount()): 
            if i%2 == 1:
                #On est sur un nom de variable
                var = self.visit(ctx.getChild(i))
                if var in self.dico_var:
                    raise Exception("Variable déjà déclarée")
                else:
                    self.dico_var[var] = "addr_" + var
                    instr += "@addr_" + var + " rmw 1\n"
    
        return instr
    

    def visitBloc(self, ctx: NiklausParser.BlocContext):
        '''
            Objectif : boucler sur chaques instructions 
            (i.e. jusqu'à ce qu'on n'ai plus d'enfant)
        '''
        instr = ""
        for i in range(ctx.getChildCount()):
            instr += self.visit(ctx.getChild(i)) + "\n"
        return instr

    def visitInstrRead(self, ctx: NiklausParser.InstrReadContext):
        '''
            A revoir
            Objectif : lire une variable
            TODO :  - getAddrVar
                    - vérifier le type de la variable
        '''
        var = "addr_"+ self.visit(ctx.getChild(1) )
        return "read r0," + var + "\n"
    
    def visitInstrWrite(self, ctx: NiklausParser.InstrWriteContext):
        '''
            A revoir
            Objectif : écrire une variable
            TODO :   - getAddrVar
                     - vérifier le type de la variable
        '''
        var = self.visit(ctx.getChild(1) )
        return "write r0, addr_" + var + "\n"
    

    def visitInstrAffect(self, ctx: NiklausParser.InstrAffectContext):
        return self.visit(ctx.getChild(0))

    def visitAffect(self, ctx: NiklausParser.AffectContext):
        '''
            Objectif : assigner une variable (pour l'instant juste int)
            TODO :   -
                     - vérifier le type de la variable
        '''
        var ="addr_"+self.visit(ctx.getChild(0))   #nom de la variable
        expr = self.visit(ctx.getChild(2))        #expression
        return expr + 'str r0, addr_'+ var + "\n"
    

    def visitInstrCond(self, ctx: NiklausParser.InstrCondContext):
        """
            Objectif : Redirige vers la fonction visitCond
            TODO :   - récupérer la condition
        """
        return self.visit(ctx.getChild(0))

    def visitCond(self, ctx: NiklausParser.CondContext):
        '''
            Objectif : if/else
            TODO :   - récupérer la condition
        '''


        comp =  self.visit(ctx.getChild(2))
        comp_type = comp.visit(NiklausParser.CompContext.getChild(1))
        bloc1 = self.visit(ctx.getChild(5))
        bloc2 = self.visit(ctx.getChild(9))

        match comp_type:
            case '<':
                return comp + "blt bloc_1 \n" + bloc2 + "b fin_test \n" + "@bloc_1 " + bloc1 + "@fin_test \n"
            case '>':
                return comp + "blt bloc_2 \n" + bloc1 + "b fin_test \n" + "@bloc_2 " + bloc2 + "@fin_test \n"
            case '=':
                return comp + "beq bloc_1 \n" + bloc2 + "b fin_test \n" + "@bloc_1 " + bloc1 + "@fin_test \n" 
            case '<=':
                return comp + "blt bloc_2 \n" + bloc1 + "b fin_test \n" + "@bloc_2 " + bloc2 + "@fin_test \n"
            case '>=':
                return comp + "blt bloc_1 \n" + bloc2 + "b fin_test \n" + "@bloc_1 " + bloc1 + "@fin_test \n"
            case '<>':
                return comp + "beq bloc_2 \n" + bloc1 + "b fin_test \n" + "@bloc_2 " + bloc2 + "@fin_test \n"
         


    def visitInstrWhile(self, ctx: NiklausParser.InstrWhileContext):
        return self.visit(ctx.getChild(0))

    def visitB_while(self, ctx: NiklausParser.InstrWhileContext):
        '''
            Objectif : while
            TODO :   - récupérer la condition
        '''
        comp =  self.visit(ctx.getChild(2)) 
        comp_type = comp.visit(NiklausParser.CompContext.getChild(1))
        bloc = self.visit(ctx.getChild(5)) 
    
        match comp_type:
            case '<': #comp de e1 et e2
                return "@boucle_start " + comp + "blt bloc \n b fin_boucle\n @bloc " + bloc + "b boucle_start" + "@fin_boucle "
            case '>': #comp de e2 et e1
                return "@boucle_start " + comp + "blt bloc \n b fin_boucle\n @bloc " + bloc + "b boucle_start" + "@fin_boucle "
            case '=':  #comp de e1 et e2
                return "@boucle_start " + comp + "beq bloc \n b fin_boucle\n @bloc " + bloc + "b boucle_start" + "@fin_boucle "
            case '<=': #comp de e2 et e1
                return "@boucle_start " + comp + "blt fin_boucle \n" + bloc + "b boucle_start" + "@fin_boucle "
            case '>=': #comp de e1 et e2
                return "@boucle_start " + comp + "blt fin_boucle \n" + bloc + "b boucle_start" + "@fin_boucle "
            case '<>': #comp de e1 et e2
                return "@boucle_start " + comp + "beq fin_boucle \n" + bloc + "b boucle_start" + "@fin_boucle "
            
    
    def visitExpr(self, ctx: NiklausParser.ExprContext):
        if self.getChildCount() == 1: #cas d'un nombre ou d'une variable
            if self.visit(ctx.getChild(0)).isdigit(): #cas d'un nombre
                return "mov r0, #" + self.visit(ctx.ctx.getChild(0)) + "\n"
            else: #cas d'une variable
                if self.visit(ctx.getChild(0)) in self.dico_var:
                    return "ldr r0, " + self.dico_var[self.visit(ctx.getChild(0))] + "\n"
                else: #cas d'une variable non déclarée
                    raise Exception("Variable non déclarée")
        else:
            match self.visit(ctx.getChild(1)):
                case '+': #cas d'une addition 
                    return self.visit(ctx.getChild(0)) +"\npush r0\n" + self.visit(ctx.getChild(2)) + "\npop r1\nadd r0, r1, r0\n"
                case '-': #cas d'une soustraction
                    return self.visit(ctx.getChild(0)) +"\npush r0\n" + self.visit(ctx.getChild(2)) + "\npop r1\nsub r0, r1, r0\n"
                case '*': #cas d'une multiplication
                    return self.visit(ctx.getChild(0)) +"\npush r0\n" + self.visit(ctx.getChild(2)) + "\npop r1\n \
                                                                                str r1, 0xAAAA\n \
                                                                                str r0, 0xAAAB\n \
                                                                                ldr r0, 0xAAAC\n"
                case '/': #cas d'une division
                    return self.visit(ctx.getChild(0)) +"\npush r0\n" + self.visit(ctx.getChild(2)) + "\npop r1\n \
                                                                                str r1, 0xAAAA\n \
                                                                                str r0, 0xAAAB\n \
                                                                                ldr r0, 0xAAAD\n"
                case 'mod': #cas d'un modulo
                    return self.visit(ctx.getChild(0)) +"\npush r0\n" + self.visit(ctx.getChild(2)) + "\npop r1\n \
                                                                                str r1, 0xAAAA\n \
                                                                                str r0, 0xAAAB\n \
                                                                                ldr r0, 0xAAAE\n"
    
    def visitComp(self, ctx: NiklausParser.CompContext):
        """
            Objectif : Comparer deux expressions
        """
        if self.getChildCount() == 1:
            Exception("Erreur de comparaison : pas assez d'arguments")
        else:
            match self.visit(ctx.getChild(1)):
                case '=':  #comp e1 et e2
                    return self.visit(ctx.getChild(0)) +"\npush r0\n" + self.visit(ctx.getChild(2)) + "\npop r1\cmp r1, r0\n"
                case '<':  #idem
                    return self.visit(ctx.getChild(0)) +"\npush r0\n" + self.visit(ctx.getChild(2)) + "\npop r1\cmp r1, r0\n"
                case '>=': #idem
                    return self.visit(ctx.getChild(0)) +"\npush r0\n" + self.visit(ctx.getChild(2)) + "\npop r1\cmp r1, r0\n"
                case '<>': #idem
                    return self.visit(ctx.getChild(0)) +"\npush r0\n" + self.visit(ctx.getChild(2)) + "\npop r1\cmp r1, r0\n"
                case '>':  #comp e2 et e1
                    return self.visit(ctx.getChild(2)) +"\npush r0\n" + self.visit(ctx.getChild(0)) + "\npop r1\cmp r0, r1\n"
                case '<=':
                    return self.visit(ctx.getChild(2)) +"\npush r0\n" + self.visit(ctx.getChild(0)) + "\npop r1\cmp r0, r1\n"


        