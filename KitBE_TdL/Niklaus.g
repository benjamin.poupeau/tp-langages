grammar Niklaus;

options { language = Python3; }

// Note : il n'y a pas besoin de nommer avec # une règle seule
// Dans ce cas, c'est comme si on l'avait nommé "Expr"
prog : nom_prog  (decl_var)?  '{'  bloc  '}' EOF;

nom_prog : 'program' ID ';' ;

decl_var : 'var' ID (',' ID)* ';' ;

bloc : (instr )+ ;

instr : read                    #InstrRead
        | write                 #InstrWrite
        | affect                #InstrAffect
        | while                 #InstrWhile
        | cond                  #InstrCond
        ;

read : 'read'  ID ';';

write : 'write' expr ';';

affect : ID ':=' expr  ';';

while : 'while'  '('  comp  ')'  '{'  bloc  '}' ;

cond : 'if'  '('  comp  ')'  '{'  bloc  '}'  'else' '{'  bloc '}' ;

expr : term (ADDOP term)* ;

term : factor (MULTOP factor)*
    ;
    
comp : factor COMPARATOR factor ;
    


factor : ID                       # FactorId
       | INT                      # FactorInt
       | '(' expr ')'             # FactorBlock
    ;


COMPARATOR :      '<'                    
                | '>'                     
                | '='                     
                | '<='                    
                | '>='                    
                | '<>'                    
                ;

INT :  [0-9]+ ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' -> skip
    ;

WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) -> skip
    ;

ADDOP   :   '+' | '-' | 'mod' ;

MULTOP  :   '*' | '/';

ID  :   [a-zA-Z_] [a-zA-Z0-0_]* ;