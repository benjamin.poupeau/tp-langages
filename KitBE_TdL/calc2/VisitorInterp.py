import sys
from antlr4 import *
from CalcParser import CalcParser
from CalcVisitor import CalcVisitor

class VisitorInterp(CalcVisitor):
    
    # Visit a parse tree produced by CalcParser#prog.
    def visitProg(self, ctx:CalcParser.ProgContext):
        return self.visit(ctx.getChild(0))
    
    # Visit a parse tree produced by CalcParser#expr.
    def visitExpr(self, ctx:CalcParser.ExprContext):
        v = 0
        i = 0
        while i < ctx.getChildCount():
            v += self.visit(ctx.getChild(i))
            i += 2
        return v

    # Visit a parse tree produced by CalcParser#monom.
    def visitMonom(self, ctx:CalcParser.MonomContext):
        v = 1
        i = 0
        while i < ctx.getChildCount():
            v *= self.visit(ctx.getChild(i))
            i += 2
        return v
        
    # Visit a parse tree produced by CalcParser#atom.
    def visitAtom(self, ctx:CalcParser.AtomContext):
        if ctx.getChildCount() == 1:
            return int(ctx.getText())
        else:
            return self.visit(ctx.getChild(1))

