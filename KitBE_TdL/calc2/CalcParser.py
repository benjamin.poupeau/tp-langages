# Generated from Calc.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\b")
        buf.write("%\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\3\2\3\2\3\2\3\3\3\3")
        buf.write("\3\3\7\3\21\n\3\f\3\16\3\24\13\3\3\4\3\4\3\4\7\4\31\n")
        buf.write("\4\f\4\16\4\34\13\4\3\5\3\5\3\5\3\5\3\5\5\5#\n\5\3\5\2")
        buf.write("\2\6\2\4\6\b\2\2\2#\2\n\3\2\2\2\4\r\3\2\2\2\6\25\3\2\2")
        buf.write("\2\b\"\3\2\2\2\n\13\5\4\3\2\13\f\7\2\2\3\f\3\3\2\2\2\r")
        buf.write("\22\5\6\4\2\16\17\7\6\2\2\17\21\5\6\4\2\20\16\3\2\2\2")
        buf.write("\21\24\3\2\2\2\22\20\3\2\2\2\22\23\3\2\2\2\23\5\3\2\2")
        buf.write("\2\24\22\3\2\2\2\25\32\5\b\5\2\26\27\7\5\2\2\27\31\5\6")
        buf.write("\4\2\30\26\3\2\2\2\31\34\3\2\2\2\32\30\3\2\2\2\32\33\3")
        buf.write("\2\2\2\33\7\3\2\2\2\34\32\3\2\2\2\35#\7\4\2\2\36\37\7")
        buf.write("\7\2\2\37 \5\4\3\2 !\7\b\2\2!#\3\2\2\2\"\35\3\2\2\2\"")
        buf.write("\36\3\2\2\2#\t\3\2\2\2\5\22\32\"")
        return buf.getvalue()


class CalcParser ( Parser ):

    grammarFileName = "Calc.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "'*'", "'+'", 
                     "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "NEWLINE", "INT", "MULT", "PLUS", "LPAR", 
                      "RPAR" ]

    RULE_prog = 0
    RULE_expr = 1
    RULE_monom = 2
    RULE_atom = 3

    ruleNames =  [ "prog", "expr", "monom", "atom" ]

    EOF = Token.EOF
    NEWLINE=1
    INT=2
    MULT=3
    PLUS=4
    LPAR=5
    RPAR=6

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(CalcParser.ExprContext,0)


        def EOF(self):
            return self.getToken(CalcParser.EOF, 0)

        def getRuleIndex(self):
            return CalcParser.RULE_prog

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProg" ):
                listener.enterProg(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProg" ):
                listener.exitProg(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProg" ):
                return visitor.visitProg(self)
            else:
                return visitor.visitChildren(self)




    def prog(self):

        localctx = CalcParser.ProgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_prog)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 8
            self.expr()
            self.state = 9
            self.match(CalcParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def monom(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CalcParser.MonomContext)
            else:
                return self.getTypedRuleContext(CalcParser.MonomContext,i)


        def PLUS(self, i:int=None):
            if i is None:
                return self.getTokens(CalcParser.PLUS)
            else:
                return self.getToken(CalcParser.PLUS, i)

        def getRuleIndex(self):
            return CalcParser.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = CalcParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 11
            self.monom()
            self.state = 16
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==CalcParser.PLUS:
                self.state = 12
                self.match(CalcParser.PLUS)
                self.state = 13
                self.monom()
                self.state = 18
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class MonomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def atom(self):
            return self.getTypedRuleContext(CalcParser.AtomContext,0)


        def MULT(self, i:int=None):
            if i is None:
                return self.getTokens(CalcParser.MULT)
            else:
                return self.getToken(CalcParser.MULT, i)

        def monom(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(CalcParser.MonomContext)
            else:
                return self.getTypedRuleContext(CalcParser.MonomContext,i)


        def getRuleIndex(self):
            return CalcParser.RULE_monom

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMonom" ):
                listener.enterMonom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMonom" ):
                listener.exitMonom(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMonom" ):
                return visitor.visitMonom(self)
            else:
                return visitor.visitChildren(self)




    def monom(self):

        localctx = CalcParser.MonomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_monom)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 19
            self.atom()
            self.state = 24
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,1,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 20
                    self.match(CalcParser.MULT)
                    self.state = 21
                    self.monom() 
                self.state = 26
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,1,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AtomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT(self):
            return self.getToken(CalcParser.INT, 0)

        def LPAR(self):
            return self.getToken(CalcParser.LPAR, 0)

        def expr(self):
            return self.getTypedRuleContext(CalcParser.ExprContext,0)


        def RPAR(self):
            return self.getToken(CalcParser.RPAR, 0)

        def getRuleIndex(self):
            return CalcParser.RULE_atom

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtom" ):
                listener.enterAtom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtom" ):
                listener.exitAtom(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAtom" ):
                return visitor.visitAtom(self)
            else:
                return visitor.visitChildren(self)




    def atom(self):

        localctx = CalcParser.AtomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_atom)
        try:
            self.state = 32
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CalcParser.INT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 27
                self.match(CalcParser.INT)
                pass
            elif token in [CalcParser.LPAR]:
                self.enterOuterAlt(localctx, 2)
                self.state = 28
                self.match(CalcParser.LPAR)
                self.state = 29
                self.expr()
                self.state = 30
                self.match(CalcParser.RPAR)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





