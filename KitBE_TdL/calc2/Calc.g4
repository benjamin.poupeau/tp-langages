grammar Calc;

prog      : expr EOF ;

expr      : monom (PLUS monom)* ;

monom     : atom (MULT monom)* ;

atom      : INT
          | LPAR expr RPAR ;


NEWLINE : [\r\n]+ -> skip;
INT     : [0-9]+ ;
MULT    : '*' ;
PLUS    : '+' ;
LPAR    : '(' ;
RPAR    : ')' ;

