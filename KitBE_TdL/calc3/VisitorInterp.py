import sys
from antlr4 import *
from CalcParser import CalcParser
from CalcVisitor import CalcVisitor

class VisitorInterp(CalcVisitor):
    
    # Visit a parse tree produced by CalcParser#prog.
    def visitProg(self, ctx:CalcParser.ProgContext):
        return self.visit(ctx.getChild(0))
    
    # Visit a parse tree produced by CalcParser#ExprMult.
    def visitExprMult(self, ctx:CalcParser.ExprMultContext):
        v1 = self.visit(ctx.getChild(0))
        v2 = self.visit(ctx.getChild(2))
        return v1 * v2

    # Visit a parse tree produced by CalcParser#ExprPlus.
    def visitExprPlus(self, ctx:CalcParser.ExprPlusContext):
        v1 = self.visit(ctx.getChild(0))
        v2 = self.visit(ctx.getChild(2))
        return v1 + v2


    # Visit a parse tree produced by CalcParser#ExprInt.
    def visitExprInt(self, ctx:CalcParser.ExprIntContext):
        return int(ctx.getText())

    # Visit a parse tree produced by CalcParser#ExprPar.
    def visitExprPar(self, ctx:CalcParser.ExprParContext):
        return self.visit(ctx.getChild(1))

