grammar Calc;

prog      : expr EOF ;

expr      : expr MULT expr   # ExprMult
          | expr PLUS expr   # ExprPlus
          | INT              # ExprInt
          | LPAR expr RPAR   # ExprPar
          ;

NEWLINE : [\r\n]+ -> skip;
INT     : [0-9]+ ;
MULT    : '*' ;
PLUS    : '+' ;
LPAR    : '(' ;
RPAR    : ')' ;

