grammar Calc;

prog      : expr EOF ;

expr      : monom expr_suff ;

expr_suff : PLUS expr
          | ;

monom     : atom monom_suf ;

monom_suf : MULT monom
          | ;

atom      : INT
          | LPAR expr RPAR ;


NEWLINE : [\r\n]+ -> skip;
INT     : [0-9]+ ;
MULT    : '*' ;
PLUS    : '+' ;
LPAR    : '(' ;
RPAR    : ')' ;

