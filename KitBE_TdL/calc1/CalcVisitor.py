# Generated from Calc.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .CalcParser import CalcParser
else:
    from CalcParser import CalcParser

# This class defines a complete generic visitor for a parse tree produced by CalcParser.

class CalcVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by CalcParser#prog.
    def visitProg(self, ctx:CalcParser.ProgContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CalcParser#expr.
    def visitExpr(self, ctx:CalcParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CalcParser#expr_suff.
    def visitExpr_suff(self, ctx:CalcParser.Expr_suffContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CalcParser#monom.
    def visitMonom(self, ctx:CalcParser.MonomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CalcParser#monom_suf.
    def visitMonom_suf(self, ctx:CalcParser.Monom_sufContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by CalcParser#atom.
    def visitAtom(self, ctx:CalcParser.AtomContext):
        return self.visitChildren(ctx)



del CalcParser