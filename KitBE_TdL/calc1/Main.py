import sys
from antlr4 import *
from CalcLexer import CalcLexer
from CalcParser import CalcParser
from VisitorInterp import VisitorInterp

def main(argv):
    input_stream = FileStream(argv[1])
    lexer = CalcLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = CalcParser(stream)
    tree = parser.prog()
    if parser.getNumberOfSyntaxErrors() > 0:
        print("syntax errors")
    else:
        vinterp = VisitorInterp()
        print(vinterp.visit(tree))

if __name__ == '__main__':
    main(sys.argv)

    
