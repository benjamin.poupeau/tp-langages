import sys
from antlr4 import *
from CalcParser import CalcParser
from CalcVisitor import CalcVisitor

class VisitorInterp(CalcVisitor):
    
    # Visit a parse tree produced by CalcParser#prog.
    def visitProg(self, ctx:CalcParser.ProgContext):
        return self.visit(ctx.getChild(0))


    # Visit a parse tree produced by CalcParser#expr.
    def visitExpr(self, ctx:CalcParser.ExprContext):
        v1 = self.visit(ctx.getChild(0))
        v2 = self.visit(ctx.getChild(1))
        return v1 + v2


    # Visit a parse tree produced by CalcParser#expr_suff.
    def visitExpr_suff(self, ctx:CalcParser.Expr_suffContext):
        if ctx.getChildCount() == 0:
            return 0
        else:
            return self.visit(ctx.getChild(1))


    # Visit a parse tree produced by CalcParser#monom.
    def visitMonom(self, ctx:CalcParser.MonomContext):
        v1 = self.visit(ctx.getChild(0))
        v2 = self.visit(ctx.getChild(1))
        return v1 * v2
        

    # Visit a parse tree produced by CalcParser#monom_suf.
    def visitMonom_suf(self, ctx:CalcParser.Monom_sufContext):
        if ctx.getChildCount() == 0:
            return 1
        else:
            return self.visit(ctx.getChild(1))
        
    # Visit a parse tree produced by CalcParser#atom.
    def visitAtom(self, ctx:CalcParser.AtomContext):
        if ctx.getChildCount() == 1:
            return int(ctx.getText())
        else:
            return self.visit(ctx.getChild(1))

