# Generated from Calc.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\b")
        buf.write(")\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2")
        buf.write("\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\4\5\4\30\n\4\3\5\3\5\3")
        buf.write("\5\3\6\3\6\3\6\5\6 \n\6\3\7\3\7\3\7\3\7\3\7\5\7\'\n\7")
        buf.write("\3\7\2\2\b\2\4\6\b\n\f\2\2\2%\2\16\3\2\2\2\4\21\3\2\2")
        buf.write("\2\6\27\3\2\2\2\b\31\3\2\2\2\n\37\3\2\2\2\f&\3\2\2\2\16")
        buf.write("\17\5\4\3\2\17\20\7\2\2\3\20\3\3\2\2\2\21\22\5\b\5\2\22")
        buf.write("\23\5\6\4\2\23\5\3\2\2\2\24\25\7\6\2\2\25\30\5\4\3\2\26")
        buf.write("\30\3\2\2\2\27\24\3\2\2\2\27\26\3\2\2\2\30\7\3\2\2\2\31")
        buf.write("\32\5\f\7\2\32\33\5\n\6\2\33\t\3\2\2\2\34\35\7\5\2\2\35")
        buf.write(" \5\b\5\2\36 \3\2\2\2\37\34\3\2\2\2\37\36\3\2\2\2 \13")
        buf.write("\3\2\2\2!\'\7\4\2\2\"#\7\7\2\2#$\5\4\3\2$%\7\b\2\2%\'")
        buf.write("\3\2\2\2&!\3\2\2\2&\"\3\2\2\2\'\r\3\2\2\2\5\27\37&")
        return buf.getvalue()


class CalcParser ( Parser ):

    grammarFileName = "Calc.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "'*'", "'+'", 
                     "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "NEWLINE", "INT", "MULT", "PLUS", "LPAR", 
                      "RPAR" ]

    RULE_prog = 0
    RULE_expr = 1
    RULE_expr_suff = 2
    RULE_monom = 3
    RULE_monom_suf = 4
    RULE_atom = 5

    ruleNames =  [ "prog", "expr", "expr_suff", "monom", "monom_suf", "atom" ]

    EOF = Token.EOF
    NEWLINE=1
    INT=2
    MULT=3
    PLUS=4
    LPAR=5
    RPAR=6

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(CalcParser.ExprContext,0)


        def EOF(self):
            return self.getToken(CalcParser.EOF, 0)

        def getRuleIndex(self):
            return CalcParser.RULE_prog

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProg" ):
                listener.enterProg(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProg" ):
                listener.exitProg(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProg" ):
                return visitor.visitProg(self)
            else:
                return visitor.visitChildren(self)




    def prog(self):

        localctx = CalcParser.ProgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_prog)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 12
            self.expr()
            self.state = 13
            self.match(CalcParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def monom(self):
            return self.getTypedRuleContext(CalcParser.MonomContext,0)


        def expr_suff(self):
            return self.getTypedRuleContext(CalcParser.Expr_suffContext,0)


        def getRuleIndex(self):
            return CalcParser.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = CalcParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 15
            self.monom()
            self.state = 16
            self.expr_suff()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Expr_suffContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PLUS(self):
            return self.getToken(CalcParser.PLUS, 0)

        def expr(self):
            return self.getTypedRuleContext(CalcParser.ExprContext,0)


        def getRuleIndex(self):
            return CalcParser.RULE_expr_suff

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr_suff" ):
                listener.enterExpr_suff(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr_suff" ):
                listener.exitExpr_suff(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr_suff" ):
                return visitor.visitExpr_suff(self)
            else:
                return visitor.visitChildren(self)




    def expr_suff(self):

        localctx = CalcParser.Expr_suffContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_expr_suff)
        try:
            self.state = 21
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CalcParser.PLUS]:
                self.enterOuterAlt(localctx, 1)
                self.state = 18
                self.match(CalcParser.PLUS)
                self.state = 19
                self.expr()
                pass
            elif token in [CalcParser.EOF, CalcParser.RPAR]:
                self.enterOuterAlt(localctx, 2)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class MonomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def atom(self):
            return self.getTypedRuleContext(CalcParser.AtomContext,0)


        def monom_suf(self):
            return self.getTypedRuleContext(CalcParser.Monom_sufContext,0)


        def getRuleIndex(self):
            return CalcParser.RULE_monom

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMonom" ):
                listener.enterMonom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMonom" ):
                listener.exitMonom(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMonom" ):
                return visitor.visitMonom(self)
            else:
                return visitor.visitChildren(self)




    def monom(self):

        localctx = CalcParser.MonomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_monom)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 23
            self.atom()
            self.state = 24
            self.monom_suf()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Monom_sufContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MULT(self):
            return self.getToken(CalcParser.MULT, 0)

        def monom(self):
            return self.getTypedRuleContext(CalcParser.MonomContext,0)


        def getRuleIndex(self):
            return CalcParser.RULE_monom_suf

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMonom_suf" ):
                listener.enterMonom_suf(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMonom_suf" ):
                listener.exitMonom_suf(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMonom_suf" ):
                return visitor.visitMonom_suf(self)
            else:
                return visitor.visitChildren(self)




    def monom_suf(self):

        localctx = CalcParser.Monom_sufContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_monom_suf)
        try:
            self.state = 29
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CalcParser.MULT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 26
                self.match(CalcParser.MULT)
                self.state = 27
                self.monom()
                pass
            elif token in [CalcParser.EOF, CalcParser.PLUS, CalcParser.RPAR]:
                self.enterOuterAlt(localctx, 2)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AtomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT(self):
            return self.getToken(CalcParser.INT, 0)

        def LPAR(self):
            return self.getToken(CalcParser.LPAR, 0)

        def expr(self):
            return self.getTypedRuleContext(CalcParser.ExprContext,0)


        def RPAR(self):
            return self.getToken(CalcParser.RPAR, 0)

        def getRuleIndex(self):
            return CalcParser.RULE_atom

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtom" ):
                listener.enterAtom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtom" ):
                listener.exitAtom(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAtom" ):
                return visitor.visitAtom(self)
            else:
                return visitor.visitChildren(self)




    def atom(self):

        localctx = CalcParser.AtomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_atom)
        try:
            self.state = 36
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [CalcParser.INT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 31
                self.match(CalcParser.INT)
                pass
            elif token in [CalcParser.LPAR]:
                self.enterOuterAlt(localctx, 2)
                self.state = 32
                self.match(CalcParser.LPAR)
                self.state = 33
                self.expr()
                self.state = 34
                self.match(CalcParser.RPAR)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





