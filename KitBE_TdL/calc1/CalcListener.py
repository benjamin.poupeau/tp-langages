# Generated from Calc.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .CalcParser import CalcParser
else:
    from CalcParser import CalcParser

# This class defines a complete listener for a parse tree produced by CalcParser.
class CalcListener(ParseTreeListener):

    # Enter a parse tree produced by CalcParser#prog.
    def enterProg(self, ctx:CalcParser.ProgContext):
        pass

    # Exit a parse tree produced by CalcParser#prog.
    def exitProg(self, ctx:CalcParser.ProgContext):
        pass


    # Enter a parse tree produced by CalcParser#expr.
    def enterExpr(self, ctx:CalcParser.ExprContext):
        pass

    # Exit a parse tree produced by CalcParser#expr.
    def exitExpr(self, ctx:CalcParser.ExprContext):
        pass


    # Enter a parse tree produced by CalcParser#expr_suff.
    def enterExpr_suff(self, ctx:CalcParser.Expr_suffContext):
        pass

    # Exit a parse tree produced by CalcParser#expr_suff.
    def exitExpr_suff(self, ctx:CalcParser.Expr_suffContext):
        pass


    # Enter a parse tree produced by CalcParser#monom.
    def enterMonom(self, ctx:CalcParser.MonomContext):
        pass

    # Exit a parse tree produced by CalcParser#monom.
    def exitMonom(self, ctx:CalcParser.MonomContext):
        pass


    # Enter a parse tree produced by CalcParser#monom_suf.
    def enterMonom_suf(self, ctx:CalcParser.Monom_sufContext):
        pass

    # Exit a parse tree produced by CalcParser#monom_suf.
    def exitMonom_suf(self, ctx:CalcParser.Monom_sufContext):
        pass


    # Enter a parse tree produced by CalcParser#atom.
    def enterAtom(self, ctx:CalcParser.AtomContext):
        pass

    # Exit a parse tree produced by CalcParser#atom.
    def exitAtom(self, ctx:CalcParser.AtomContext):
        pass


