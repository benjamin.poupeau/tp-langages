#!/usr/bin/python

import sys, getopt, re

######################################################################
### LL library
### 

class Shape:
    """Regular expressions for tokens in lexer definition.

Tokens are modeled with two classes: Shape (this class) and Token.

This class aims at abstracting the structure of token definitions in
grammars, i.e. its name, the regular expression characterizing it, and
a layman description of it.

Typical use-case for Shape:

   DEC = Shape("DEC", "[0-9]+", "a number")
   DEC.match("12 apples")

which outputs the string "12"


To note: Shape equality (and hash) is based its name, and one can
equate Shape and Token objects.

    """
    def __init__(self, name, re, description):
        self.re = re
        self.name = name
        self.description = description

    def match(self,s):
        """Return the longest prefix of s matching self.re, or None if no
match was found.""" 
        m = re.match(self.re,s)
        if m:
            return s[:m.end()]
        else:
            return None

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)

    def __eq__(self,x):
        if isinstance(x,EOF):
            return False
        elif isinstance(x,Shape):
            return self.name == x.name
        elif isinstance(x,str):
            return self.name == x
        else:
            raise TypeError(f"Shape equality undefined for {type(x)}")

class Token(Shape):
    """A recognized Shape: contains also line and column numbers, and the
recognized value

Equality on tokens an equivalence relation: two tokens are equals if
they have the same shape.

A special Token is defined for the end of file: it is defined using
the subclass EOF.

    """
    
    def __init__(self, shape, line, column, val):
        super().__init__(shape.name,shape.re,shape.description)
        self.line = line
        self.column = column
        self.val = val

    def isEOF(self):
        return False

    def __str__(self):
        return f"{self.name}({self.line},{self.column},'{self.val}')" 


class EOF(Token):
    """Special token for the end of file.

Two EOF objects are always equal, and any other token or shape can
only be equate an EOF object if they are themselve EOF objects.

    """
    def __init__(self):
        self.description = "EOF (the end of file)"

    def isEOF(self):
        return True

    def __str__(self):
        return f"[EOF]"
    
    def __eq__(self,x):
        return isinstance(x,EOF)


class RecognitionError(Exception):
    """Error produced during the lexing phase."""
    def __init__(self,lexer,message):
        super().__init__(message)
        self.lexer = lexer

class Lexer:
    """Encapsulating the lexing phase.

A lexer consists in an ordered list of shapes. At each step, the
"winning" shape is the one recognizing the longest prefix. In case of
tie, the first one appearing in the list is the chosen one.

A lexer is defined with 
- The list of Shape defining all possible tokens
- The Shape of tokens to not show in the stream of tokens

Typical use-case:

    DEC = Shape("DEC", "[0-9]+",    "a number")
    OP  = Shape("OP",  "([+]|[-])", "an operation")
    WS  = Shape("WS",  "[ ]", "a whitespace")
    
    lexer = Lexer([WS,DEC,OP], [WS])
    
    stream = lexer.tokenize("12 + 34 - 56")
    
    for tok in stream:
       print(tok)
       if tok == EOF():
           break
    
which produces

    DEC(1,1,'12')
    OP(1,4,'+')
    DEC(1,6,'34')
    OP(1,9,'-')
    DEC(1,11,'56')
    [EOF]

    """
    def __init__(self, shapes, tohide):
        """shape is a list of allowed shape, tohide contains the shapes to
ignore and not show in the token stream.

        """
        self.string = None
        self.shapes = shapes
        self.tohide = set(tohide)
        self.idx = 0
        self.line = 1
        self.column = 1
        self.token = None

    def _updateLoc(self,s):
        for i in range(len(s)):
            self.idx += 1
            if s[i] == "\n":
                self.line += 1
                self.column = 1
            else:
                self.column += 1
    
    def tokenize(self, s):
        """Make a stream (an Iterator) of Token out of the string s. Shapes
defined in self.tohide are ignored. When the end of the string is met,
the stream only returns EOF objects.

        """
        self.string = s
        while self.idx < len(s):
            candidate_string = ""
            candidate_shape = None
            for shape in range(len(self.shapes)):
                try:
                    prefix = self.shapes[shape].match(self.string[self.idx:])
                except re.error:
                    raise RecognitionError(self, f"The regexp of shape {self.shapes[shape]} seems to be buggy.")
                if prefix:
                    if len(prefix) > len(candidate_string):
                        candidate_string = prefix
                        candidate_shape = shape
            if len(candidate_string) == 0:
                self.token = None
                raise RecognitionError(self, f"Lexer got stuck at line {self.line}, column {self.column}.")
            elif self.shapes[candidate_shape] in self.tohide:
                self._updateLoc(candidate_string)
            else:
                self.token = Token(self.shapes[candidate_shape], self.line, self.column, candidate_string)
                self._updateLoc(candidate_string)
                yield self.token

        while True:
            yield EOF()


class ParserWarning(Exception):
    """Parser warnings. Helper class used by Parser.warn(). Stored as an
exception so that it can be raised.

    """

    Error = 3
    Print = 2
    Hide  = 1

    def __init__(self,line,column,error,mitigate):
        super().__init__(f"line {line}, column {column}: {error}")
        self.error = error
        self.mitigate = mitigate
        self.line = line
        self.column = column

    def show(self):
        print(f"Warning: line {self.line}, column {self.column}: {self.error}. {self.mitigate}.", file = sys.stderr)

class ParserError(Exception):
    def __init__(self,parser,message):
        super().__init__(message)
        self.parser = parser

class Parser():
    """Encapsulate the parsing phase of a recursive descent, LL
parser. This class should be used as superclass for a parser.

Typical use-case:

    DEC = Shape("DEC", "[0-9]+",    "a number")
    VAR = Shape("VAR", "[a-z]",     "a variable")
    OP  = Shape("OP",  "([+]|[-])", "an operation")
    WS  = Shape("WS",  "[ ]", "a whitespace")
    
    calcLexer = Lexer([WS,DEC,VAR,OP], [WS])
    
    class CalcParser(Parser):
        def __init__(self,string):
            super().__init__(calcLexer,string)
    
        def term(self):
            tok = self.peek()
            if tok == VAR:
                self.match(VAR)
                return
            elif tok == DEC:
                self.match(DEC)
                return
            else:
                raise Exception(f"Parser Error on line {tok.line}, col {tok.column}. Unexpected token: {tok.description} ('{tok.val}')")
        
        def expr(self):
            self.term()
            tok = self.peek()
            if tok == EOF():
                return
            else:
                self.match(OP)
                self.expr()
    
    parser = CalcParser("1 + 2 + x")
    parser.expr()
    
    

    """
    
    def __init__(self,lexer,string,Wlevel=ParserWarning.Print):
        self.lexer = lexer
        self.tokens = lexer.tokenize(string)
        self.Wlevel = Wlevel # Uses static values in ParserWarning
        self.warnings = []   # log the warnings
        
        # Fifo of peeked tokens
        self.buf = [None] # Dynamic size, depending on peeking
        self.beg = 0      # where to queue
        self.end = 0      # where to dequeue
        
        
    def prev(self):
        """Return the previous token in the stream"""
        return self.buf[self.beg]
        
    def warn(self,error,mitigate):
        w = None
        if self.prev():
            w = ParserWarning(self.prev().line,self.prev().column,error,mitigate)
        else:
            w = ParserWarning(1,1,error,mitigate)
        if self.Wlevel == ParserWarning.Error:
            raise w
        elif self.Wlevel == ParserWarning.Print:
            w.show()
        else:
            self.warnings.append(w)
    
    def _showBuf(self):
        print()
        print("=== BEG Buffer state ===")
        print(f"size : {len(self.buf)}, beg = {self.beg}, end = {self.end}")
        aux = "buf = "
        for i in range(len(self.buf)):
            aux += str(self.buf[i]) + " "
        print(aux)
        print("=== END Buffer state ===")
        print()
        
    def peek(self, n=1):
        offset = 0
        bufSize = len(self.buf)
        while self.end != (self.beg + offset) % bufSize:
            offset += 1

        if n >= bufSize: # Need to make a larger bufSize
            newbufSize = bufSize
            while newbufSize <= n:
                newbufSize = 2 * newbufSize
            newbuf = [None for i in range(newbufSize)]
            for i in range(offset):
                newbuf[i] = self.buf[(self.beg+i) % bufSize]
            bufSize = newbufSize
            self.buf = newbuf
            self.beg = 0
            self.end = offset
        
        if offset >= n:
            tok = self.buf[(self.beg + n - 1) % bufSize]
            return tok
        else:
            for i in range(n - offset):
                tok = next(self.tokens)
                self.buf[self.end] = tok
                self.end = (self.end + 1) % bufSize
            return tok
    
    def pop(self):
        tok = self.peek()
        self.beg = (self.beg + 1) % len(self.buf)
        return tok

    def match(self,shape):
        tok = self.pop()
        if tok == shape:
            return tok
        else:
            raise ParserError(self,f"line {tok.line}, column {tok.column}: Expected {shape.description}, was given {tok.description}")

    def matchOrPass(self,shape):
        tok = self.peek()
        if tok == shape:
            return self.pop()
        else:
            return None
    
    def dispatch(self, switch, default, *arg):
        """To simulate case distinction. 
- switch : dictionary mapping 
      - Shape to lambda's,
      - a "default" case to a string explaining what is wrong.
- default : the "default" key.
- *arg : the arguments passed to the lambda's.

Typical use-case: instead of the method term() shown in the docstring
of the class Parser, one can write:

    def term(self):
        self.dispatch({
            VAR : lambda: self.match(VAR),
            DEC : lambda: self.match(DEC),
            "_" : "Expected a variable or a number."
        }, "_")

        """
        tok = self.peek()
        if tok in switch:
            return switch[tok](*arg)
        else:
            raise ParserError(self, f"line {tok.line}, column {tok.column}: found what I recognize as {tok.description} ('{tok.val}'). " + switch[default])
    


def _useCaseParser():
    DEC = Shape("DEC", "[0-9]+",    "a number")
    VAR = Shape("VAR", "[a-z]",     "a variable")
    OP  = Shape("OP",  "([+]|[-])", "an operation")
    WS  = Shape("WS",  "[ ]", "a whitespace")
    
    calcLexer = Lexer([WS,DEC,VAR,OP], [WS])
    
    class CalcParser(Parser):
        def __init__(self,string):
            super().__init__(calcLexer,string)
    
        def term(self):
            tok = self.peek()
            if tok == VAR:
                self.match(VAR)
                return
            elif tok == DEC:
                self.match(DEC)
                return
            else:
                raise Exception(f"Parser Error on line {tok.line}, col {tok.column}. Unexpected token: {tok.description} ('{tok.val}')")

        def term(self):
            self.dispatch({
                VAR : lambda: self.match(VAR),
                DEC : lambda: self.match(DEC),
                "_" : "Expected a variable or a number."
            }, "_")

        def expr(self):
            self.termbis()
            tok = self.peek()
            if tok == EOF():
                return
            else:
                self.match(OP)
                self.expr()
    
    parser = CalcParser("1 + 2 + x")
    parser.expr()



######################################################################
### asmDslCore
### 

from collections.abc import Iterable

class RegisterNberException(Exception):
    def __init__(self,r):
        super().__init__(f"Register index can only be a number 0-7. Got instead '{str(r)}'.")
        self.r = r

class TypeRegisterException(Exception):
    def __init__(self,r):
        super().__init__(f"\n  Expected a register (of type Reg).\n  Got instead '{str(r)}' of type '{type(r)}'.")
        self.r = r

class Reg():
    """class to encapsulate registers. 

There are only 8 registers, numbered 0 to 7: the constructor takes the
corresponding number."""
    
    def __init__(self,n):
        if isinstance(n,int) and (n < 8 and n >= 0):
            self.n = n
        else:
            raise RegisterNberException(n)

    def __str__(self):
        return f"r{self.n}"
    
    def __eq__(self,other):
        return self.n == other.n


class Symb():
    """Should only be used through its subclasses Label and
Word. Encapsulate what kind of immediate values can be given to
instructions.

    """
    def val(self,d):
        """Return the word (as an integer) associated with the symbol. Inputs
a dictonary mapping labels to words.

Raises MissingLabelException when called on Label not available in d.

        """
        raise Exception("Should be used with its subclasses")

class MissingLabelException(Exception):
    def __init__(self,label):
        super().__init__(f"Label '{str(label)}' undefined.")
        self.label = label

class TypeLabelException(Exception):
    def __init__(self,label):
        super().__init__(f"\n  A label can only be a string with no spaces and that does not look like a register name.\n  Got '{str(label)}' of type '{type(label)}' instead.")
        self.label = label

class Label(Symb):
    """Class to encapsulate label symbols.

Label(name) generates a fresh name for you, or take the name (as a
string) you provide. Two labels are equals if they have the same name:
several invocation to Label("blah") are allowed and refer to the same
label.

Label(prefix="blah") generates a label with a unique name starting with
blah. So two invocations to Label(prefix="blah") will give distinct
labels.

Label() can also be invoked without argument: a default prefix is then
used.

Raises TypeLabelException if the provided name is not a string or an
invalid string.
    """
    _existing = set()
    _idx = 0
    def __init__(self,name=None,prefix=""):
        self.name = ""
        if name == None:
            n = f"label_{prefix}{Label._idx}"
            while n in Label._existing:
                Label._idx += 1
                n = f"label_{prefix}{Label._idx}"
            self.name = n
        else:
            if isinstance(name,str) \
               and not re.match("^r[0-9]+$", name) \
               and not re.match("[^ \t\n\r]*[ \t\n\r]", name): # Should not contain r+number nor spaces
                self.name = name
            else:
                raise TypeLabelException(name)
        Label._existing.add(self.name)
    
    def __str__(self):
        return self.name

    def __eq__(self,other):
        return str(self) == str(other)

    def __hash__(self):
        return hash(str(self))
    
    def val(self,d):
        if self.name not in d:
            raise MissingLabelException(self.name)
        return d[self.name]


class TypeWordException(Exception):
    def __init__(self,w):
        super().__init__(f"\n  A word can only be an integer between 0 and 2^16-1 .\n  Got '{str(w)}' of type '{type(w)}' instead.")
        self.w = w

class Word(Symb):
    """To encapsulate 16-long bitstring, stored as unsigned integer."""
    
    def __init__(self,w):
        """Expect an integer between 0 and 2^16"""
        if  isinstance(w,int) and (w >= 0 and w < 65536):
            self.w = w
        else:
            raise TypeWordException(w)

    def __str__(self):
        return f"0x{ASM.hex(self.w)}"

    def __eq__(self,other):
        return str(self) == str(other)

    def val(self,d):
        return self.w


class ASMinstr():
    def __init__(self,*arg):
        self.arg = arg
    
    def __str__(self):
        return ASMstrDict[self.__class__](*self.arg)

    def _toWord(code,imm,rx,ry,rz):
        res = code
        res = (res << 1) + imm
        res = (res << 3) + rx
        res = (res << 3) + ry
        res = (res << 3) + rz
        res = res << 2 
        return res

    def compile(self,mapLabel):
        return ASMcompDict[self.__class__](mapLabel,*self.arg)

    def size(self):
        return ASMsizeDict[self.__class__](*self.arg)


class ASMldrInd(ASMinstr): pass
class ASMldrImm(ASMinstr): pass
class ASMstrInd(ASMinstr): pass
class ASMstrImm(ASMinstr): pass
class ASMmovInd(ASMinstr): pass
class ASMmovImm(ASMinstr): pass
class ASMaddInd(ASMinstr): pass
class ASMaddImm(ASMinstr): pass
class ASMsubInd(ASMinstr): pass
class ASMsubImm(ASMinstr): pass
class ASMcmpInd(ASMinstr): pass
class ASMcmpImm(ASMinstr): pass
class ASMbeqInd(ASMinstr): pass
class ASMbeqImm(ASMinstr): pass
class ASMbltInd(ASMinstr): pass
class ASMbltImm(ASMinstr): pass
class ASMjmpInd(ASMinstr): pass
class ASMjmpImm(ASMinstr): pass
class ASMpush(ASMinstr): pass
class ASMpop(ASMinstr):  pass
class ASMrmw(ASMinstr):  pass
class ASMsmwInt(ASMinstr):  pass
class ASMsmwLab(ASMinstr):  pass
class ASMsmwStr(ASMinstr):  pass
class ASMsmwList(ASMinstr):  pass


def _str_of_word_list(l):
    s = "["
    for i in range(len(l)):
        s += str(l[i]) + ", "
    return (s[:-2] + "]")

class TypeStringException(Exception):
    def __init__(self,message):
        super().__init__(message)

def encode_string(s):
    out = ""
    for i in range(len(s)):
        if ord(s[i]) == 10: # \n
            out += "\\n"
        elif ord(s[i]) == 13: # \r -- dropping this one
            pass
        elif ord(s[i]) == 9: # \t
            out += "\\t"
        elif ord(s[i]) == 92: # backslash
            out += "\\\\"
        elif ord(s[i]) in set([34, 39]): # ' or "
            out += "\\" + s[i]
        elif ord(s[i]) >= 32 and ord(s[i]) <= 126: # all other ascii characters
            out += s[i]
        else:
            raise TypeStringException(f"Allowed characters: ASCII codes 32 to 126, with 9, 10 and 13 (tab and newline). Instead, was given '{s[i]}' of code {ord(s[i])}")
    return out

def decode_string(s):
    out = ""
    i = 0
    while i < len(s):
        if ord(s[i]) == 92: # found backslash
            i += 1
            if i >= len(s):
                raise TypeStringException("Escaping character ('\\') found alone")
            elif s[i] == "n":
                out += "\n"
            elif s[i] == "t":
                out += "\t"
            elif s[i] == "\\":
                out += "\\"
            elif s[i] == "'":
                out += "'"
            elif s[i] == '"':
                out += '"'
            else:
                raise TypeStringException(f"Allowed escaped characters: '\\n', '\\t', '\\n', '\\'', '\\\"'. Instead, was given '{s[i]}' of code {ord(s[i])}")
        else:
            out += s[i]
        i += 1
    return out

ASMstrDict = {
    ASMldrInd : lambda rx,ry: f"ldr {rx}, [{ry}]",
    ASMldrImm : lambda rx,ad: f"ldr {rx}, {ad}",
    ASMstrInd : lambda rz,ry: f"str {rz}, [{ry}]",
    ASMstrImm : lambda rz,ad: f"str {rz}, {ad}",
    ASMmovInd : lambda rx,ry: f"mov {rx}, {ry}",
    ASMmovImm : lambda rx,ad: f"mov {rx}, #{ad}",
    ASMaddInd : lambda rx,ry,rz: f"add {rx}, {ry}, {rz}",
    ASMaddImm : lambda rx,ry,ad: f"add {rx}, {ry}, #{ad}",
    ASMsubInd : lambda rx,ry,rz: f"sub {rx}, {ry}, {rz}",
    ASMsubImm : lambda rx,ry,ad: f"sub {rx}, {ry}, #{ad}",
    ASMcmpInd : lambda ry,rz: f"cmp {ry}, {rz}",
    ASMcmpImm : lambda ry,ad: f"cmp {ry}, #{ad}",
    ASMbeqInd : lambda rz: f"beq {rz}",
    ASMbeqImm : lambda ad: f"beq {ad}",
    ASMbltInd : lambda rz: f"blt {rz}",
    ASMbltImm : lambda ad: f"blt {ad}",
    ASMjmpInd : lambda rz: f"b {rz}",
    ASMjmpImm : lambda ad: f"b {ad}",
    
    ASMpush : lambda r: f"push {r}",
    ASMpop :  lambda r: f"pop {r}",
    ASMrmw :  lambda v: f"rmw {v}",
    ASMsmwInt :  lambda v: f"smw {v}",
    ASMsmwLab :  lambda v: f"smw {v}",
    ASMsmwStr :  lambda v: f"smw '{encode_string(v)}'",
    ASMsmwList :  lambda v: f"smw {_str_of_word_list(v)}"
}

ASMsizeDict = {
    ASMldrInd : lambda *x: 1,  ASMldrImm : lambda *x: 2,
    ASMstrInd : lambda *x: 1,  ASMstrImm : lambda *x: 2,
    ASMmovInd : lambda *x: 1,  ASMmovImm : lambda *x: 2,
    ASMaddInd : lambda *x: 1,  ASMaddImm : lambda *x: 2,
    ASMsubInd : lambda *x: 1,  ASMsubImm : lambda *x: 2,
    ASMcmpInd : lambda *x: 1,  ASMcmpImm : lambda *x: 2,
    ASMbeqInd : lambda *x: 1,  ASMbeqImm : lambda *x: 2,
    ASMbltInd : lambda *x: 1,  ASMbltImm : lambda *x: 2,
    ASMjmpInd : lambda *x: 1,  ASMjmpImm : lambda *x: 2,
    ASMpush : lambda r: 3,
    ASMpop :  lambda r: 3,
    ASMrmw :  lambda v: v,
    ASMsmwInt :   lambda v: 1,
    ASMsmwLab :   lambda v: 1,
    ASMsmwStr :   lambda v: len(v)+1,
    ASMsmwList :  lambda v: len(v),
}

ASMcompDict = {
    ASMldrInd : lambda m,rx,ry:    [ASMinstr._toWord( 0, 0, rx.n, ry.n,    0)           ],
    ASMldrImm : lambda m,rx,ad:    [ASMinstr._toWord( 0, 1, rx.n,    0,    0), ad.val(m)],
    ASMstrInd : lambda m,rz,ry:    [ASMinstr._toWord( 1, 0,    0, ry.n, rz.n)           ],
    ASMstrImm : lambda m,rz,ad:    [ASMinstr._toWord( 1, 1,    0,    0, rz.n), ad.val(m)],
    ASMmovInd : lambda m,rx,ry:    [ASMinstr._toWord( 2, 0, rx.n, ry.n,    0)           ],
    ASMmovImm : lambda m,rx,ad:    [ASMinstr._toWord( 2, 1, rx.n,    0,    0), ad.val(m)],
    ASMaddInd : lambda m,rx,ry,rz: [ASMinstr._toWord( 3, 0, rx.n, ry.n, rz.n)           ],
    ASMaddImm : lambda m,rx,ry,ad: [ASMinstr._toWord( 3, 1, rx.n, ry.n,    0), ad.val(m)],
    ASMsubInd : lambda m,rx,ry,rz: [ASMinstr._toWord( 4, 0, rx.n, ry.n, rz.n)           ],
    ASMsubImm : lambda m,rx,ry,ad: [ASMinstr._toWord( 4, 1, rx.n, ry.n,    0), ad.val(m)],
    ASMcmpInd : lambda m,ry,rz:    [ASMinstr._toWord( 5, 0,    0, ry.n, rz.n)           ],
    ASMcmpImm : lambda m,ry,ad:    [ASMinstr._toWord( 5, 1,    0, ry.n,    0), ad.val(m)],
    ASMbeqInd : lambda m,rz:       [ASMinstr._toWord( 6, 0,    0,    0, rz.n)           ],
    ASMbeqImm : lambda m,ad:       [ASMinstr._toWord( 6, 1,    0,    0,    0), ad.val(m)],
    ASMbltInd : lambda m,rz:       [ASMinstr._toWord( 7, 0,    0,    0, rz.n)           ],
    ASMbltImm : lambda m,ad:       [ASMinstr._toWord( 7, 1,    0,    0,    0), ad.val(m)],
    ASMjmpInd : lambda m,rz:       [ASMinstr._toWord( 8, 0,    0,    0, rz.n)           ],
    ASMjmpImm : lambda m,ad:       [ASMinstr._toWord( 8, 1,    0,    0,    0), ad.val(m)],
    ASMpush :   lambda m,r: ASMaddImm(Reg(6),Reg(6),Word(1)).compile(m) + ASMstrInd(r,Reg(6)).compile(m),
    ASMpop :    lambda m,r: ASMldrInd(r,Reg(6)).compile(m) + ASMsubImm(Reg(6),Reg(6),Word(1)).compile(m),
    ASMrmw :    lambda m,v: [0 for i in range(v)],
    ASMsmwInt :    lambda m,v: [v.val(m)],
    ASMsmwLab :    lambda m,v: [v.val(m)],
    ASMsmwStr :    lambda m,v: list(map(lambda x: ord(x), v)) + [0],
    ASMsmwList :   lambda m,v: list(map(lambda x: x.val(m), v))
}


class DuplicateLabelException(Exception):
    def __init__(self,id1,asm1,id2,asm2,label):
        super().__init__(f"""
  Label '{label}' already defined: I just tried to link it to the new instruction
  '{asm1.insts[id1]}' but it was already set to index {id2} with instruction '{asm2.insts[id2]}'.""")
        self.asm1 = asm1
        self.asm2 = asm2
        self.id1 = id1
        self.id2 = id2
        self.label = label

class ASMcore():
    """Encapsulate the writing of ASM programs. You should be using the class ASM instead.""" 

    def __init__(self):
        self.insts = [] # List of instructions
        self.loc = []   # List of position in the memory for each corresponding instruction
        self.nextLoc = 0
        self.labels = {} # maps label names to indices of self.insts and self.loc
        self.labelWaiting = set() # label(s) waiting to be placed on the next instruction

    def hex(i):
        """static function that makes a 4-characters hexa word out of a
(non-negative) integer between 0 and 65535.

        """
        if not (isinstance(i,int) and i < 65536 and i >= 0):
            raise OverflowError(f"Expected an integer between 0 and 2^16-1. Got '{i}' of type {type(i)}.")
        return "{0:04x}".format(i).upper()
    
    def show(self,filename=None):
        """Printout the instructions in ASM format."""
        f = sys.stdout
        if filename:
            f = open(filename, 'w')

        for i in range(len(self.insts)):
            s = str(self.insts[i])
            for l in self.labels:
                if self.labels[l] == i:
                    s = "@" + str(l) + " " + s
            print(s,file=f)

        if filename:
            f.close()

    def compile(self):
        """Make a list of words (as int) out of the current list of
instructions. In particular, instantiate the existing labels into
locations, and complain if there is a problem.

        """
        code = []
        d = {}
        for l in self.labels:
            d[l] = self.loc[self.labels[l]]
        for id in range(len(self.insts)):
            inst = self.insts[id]
            code += inst.compile(d)
        return code

    def showMem(self,filename=None):
        """Run "compile" and print the list of words in hexadecimal, ready to
be ingested by logisim.

        """

        f = sys.stdout
        if filename:
            f = open(filename, 'w')
        
        print("v2.0 raw",file=f)
        
        c = self.compile()

        col = 0
        for i in range(len(c)):
            f.write(ASM.hex(c[i]))
            if col < 7:
                if i < len(c) - 1:
                    f.write(" ")
                col += 1
            else:
                if i < len(c) - 1:
                    f.write("\n")
                col = 0

        if filename:
            f.close()

    def showLst(self,filename=None):
        """Run "compile" and then nicely print the list of words in
hexadecimal together with the corresponding instruction, and label
correspondance.

        """
        loc_to_inst = {}
        for i in range(len(self.loc)):
            loc_to_inst[self.loc[i]] = i

        loc_to_label = {}
        for label in self.labels:
            loc = self.loc[self.labels[label]]
            if loc in loc_to_label:
                loc_to_label[loc].add(label)
            else:
                loc_to_label[loc] = set([label])
        
        f = sys.stdout
        if filename:
            f = open(filename, 'w')

        c = self.compile()
        for i in range(len(c)):
            s = ""
            s += ASM.hex(i)
            s += " - "
            s += ASM.hex(c[i])
            if i in loc_to_inst:
                s += " - "
                sinst = str(self.insts[loc_to_inst[i]])
                if i in loc_to_label:
                    s += sinst.ljust(30, ' ')
                    s += "- "
                    for lab in loc_to_label[i]:
                        s += "@" + str(lab) + " "
                else:
                    s += sinst
            print(s, file=f)
        if filename:
            f.close()
    
    def setFutureLabel(self,label):
        """Store the label and will attach it to the next instruction to be added to self."""
        if isinstance(label,Label):
            self.labelWaiting.add(label)
        elif isinstance(label,str):
            self.labelWaiting.add(Label(label))
        else:
            raise TypeLabelException(label)
        
    def append(self,inst,label=None):
        """Add the instruction inst to the list of instructions, together with
the provided label. Consider using the dedicated methods ldr, str,
etc.

Might raise DuplicateLabelException, TypeLabelException.

        """
        self.insts.append(inst)
        self.loc.append(self.nextLoc)
        # Start with the labels waiting
        for lab in self.labelWaiting:
            if lab in self.labels:
                raise DuplicateLabelException(len(self.insts)-1, self, self.labels[lab], self, lab)
            self.labels[lab] = len(self.insts)-1
        self.labelWaiting = set()
        # Add the provided labels
        if label:
            if isinstance(label,Label) or isinstance(label,str):
                if label in self.labels:
                    raise DuplicateLabelException(len(self.insts)-1, self, self.labels[label], self, label)
                self.labels[label] = len(self.insts)-1
            elif isinstance(label,Iterable):
                for lab in label:
                    if isinstance(lab,Label) or isinstance(lab,str):
                        if lab in self.labels:
                            raise DuplicateLabelException(len(self.insts)-1, self, self.labels[lab], self, lab)
                        self.labels[lab] = len(self.insts)-1
                    else:
                        raise TypeLabelException(lab)
            else:
                raise TypeLabelException(label)
                        
        self.nextLoc += inst.size()

    def concat(self,asm):
        """Concatenate the ASM object asm at the end of self."""
    
        for label in asm.labels:
            if label in self.labels:
                raise DuplicateLabelException(asm.labels[label], asm, self.labels[label], self, label)
            self.labels[label] = len(self.insts) + asm.labels[label]
        
        for inst in range(len(asm.insts)):
            self.insts.append(asm.insts[inst])

        for idx in range(len(asm.loc)):
            self.loc.append(asm.loc[idx] + self.nextLoc)

        self.nextLoc += asm.nextLoc
    
    def _append(self,label,ImmOp, IndOp, suff, *pre):
        for i in range(len(pre)):
            if not isinstance(pre[i],Reg):
                raise TypeRegisterException(pre[i])
        if   isinstance(suff, Symb): self.append(ImmOp(*pre,suff),label=label)
        elif isinstance(suff, int):  self.append(ImmOp(*pre,Word(suff)),label=label)
        elif isinstance(suff, Reg):  self.append(IndOp(*pre,suff),label=label)
        else: raise TypeError(f"Expected Reg, Label, Word or int. Got '{suff}' of type {type(suff)}.")


    def ldr(self,rx,src,label=None):

        """Append instruction ldr"""; self._append(label,ASMldrImm,ASMldrInd,src,rx)
    def str(self,rz,dest,label=None):   """Append instruction str"""; self._append(label,ASMstrImm,ASMstrInd,dest,rz)
    def mov(self,rx,src,label=None):    """Append instruction mov"""; self._append(label,ASMmovImm,ASMmovInd,src,rx)
    def add(self,rx,ry,src,label=None): """Append instruction add"""; self._append(label,ASMaddImm,ASMaddInd,src,rx,ry)
    def sub(self,rx,ry,src,label=None): """Append instruction sub"""; self._append(label,ASMsubImm,ASMsubInd,src,rx,ry)
    def cmp(self,ry,src,label=None):    """Append instruction cmp"""; self._append(label,ASMcmpImm,ASMcmpInd,src,ry)
    def beq(self,src,label=None):       """Append instruction beq"""; self._append(label,ASMbeqImm,ASMbeqInd,src)
    def blt(self,src,label=None):       """Append instruction blt"""; self._append(label,ASMbltImm,ASMbltInd,src)
    def b(self,src,label=None):         """Append instruction b""";   self._append(label,ASMjmpImm,ASMjmpInd,src)
    def jmp(self,src,label=None):       """Append instruction b (alias of b)"""; self.b(src,label=label)

    def push(self,rx,label=None):
        """Append instruction macro push."""
        if isinstance(rx,Reg):
            self.append(ASMpush(rx),label=label)
        else:
            raise TypeRegisterException(rx)
        
    def pop(self,rx,label=None):
        """Append instruction macro pop."""
        if isinstance(rx,Reg):
            self.append(ASMpop(rx),label=label)
        else:
            raise TypeRegisterException(rx)

    def smw(self,v,label=None):
        """Reserve contiguous words to store value v. 

- If v is an integer: should fit on 16 bits
- If v is a string of size n: n+1 consecutive words are used, the last
  one being at 0.
- If v is a length-n list of integers: n consecutive words are
  used. Each int should fit on 16 bits

        """
        if isinstance(v,int):
            self.append(ASMsmwInt(Word(v)),label=label)
        elif isinstance(v,Word):
            self.append(ASMsmwInt(v),label=label)
        elif isinstance(v,Label):
            self.append(ASMsmwLab(v),label=label)
        elif isinstance(v,str):
            self.append(ASMsmwStr(v),label=label)
        elif isinstance(v,Iterable): #################### THIS NEEDS TO BE CLEANED: MISSING LABELS
            l = []
            for i in range(len(v)):
                if isinstance(v[i],Word):
                    l.append(v[i])
                elif isinstance(v[i],Label):
                    l.append(v[i])
                elif isinstance(v[i],int):
                    l.append(Word(v[i]))
                else:
                    raise TypeError(f"smw can only handle ints and labels in lists. Got '{v[i]}' of type {type(v[i])}.")
            self.append(ASMsmwList(l),label=label)
        else:
            raise TypeError(f"smw expects int, Label, str or list of int/labels. Got '{v}' of type {type(v)}.")

    def rmw(self,v,label=None):
        """Reserve v contiguous, empty words"""
        if isinstance(v,int) and v > 0:
            self.append(ASMrmw(v),label=label)
        else:
            raise IndexError(f"rwm expect a positive int. Got '{v}' of type {type(v)}.")





######################################################################
### Lexer and Parser of assembler
### 


class T: pass

T.WS =      Shape("WS",      "[ \t\n\r]+", "a whitespace")
T.COMMENT = Shape("COMMENT", "%.*[\n\r]+", "a comment")
T.REG =     Shape("REG",    "[rR][0-9]+", "a register name")
T.ALIAS =   Shape("ALIAS",   "@[a-zA-Z_][a-zA-Z_0-9]*", "a label definition")
T.HASH =    Shape("HASH",    "#", "a hash ('#')")
T.LDR =     Shape("LDR",     "[lL][dD][rR]", "the instruction ldr")
T.STR =     Shape("STR",     "[sS][tT][rR]", "the instruction str")
T.MOV =     Shape("MOV",     "[mM][oO][vV]", "the instruction mov")
T.ADD =     Shape("ADD",     "[aA][dD][dD]", "the instruction add")
T.SUB =     Shape("SUB",     "[sS][uU][bB]", "the instruction sub")
T.CMP =     Shape("CMP",     "[cC][mM][pP]", "the instruction cmp")
T.BEQ =     Shape("BEQ",     "[bB][eE][qQ]", "the instruction beq")
T.BLT =     Shape("BLT",     "[bB][lL][tT]", "the instruction blt")
T.JMP =     Shape("JMP",     "([bB]|[jJ][mM][pP])", "the instruction b (or jmp)")
T.POP =     Shape("POP",     "[pP][oO][pP]", "the instruction pop")
T.PUSH =    Shape("PUSH",    "[pP][uU][sS][hH]", "the instruction push")
T.SMW =     Shape("SMW",     "[sS][mM][wW]", "the allocation instruction smw")
T.RMW =     Shape("RMW",     "[rR][mM][wW]", "the allocation instruction rmw")
T.DEC =     Shape("DEC",     "[0-9]+", "a non-negative integer in decimal")
T.HEX =     Shape("HEX",     "0x[0-9a-fA-F]+", "a non-negative integer in hexadecimal")
T.STRING =  Shape("STRING",  "\"([^\"\\\\]|\\\\\"|\\\\n|\\\\r|\\\\t)+\"", "a string")
T.LBRA =    Shape("LBRA",    "\[", "an opening square bracket")
T.RBRA =    Shape("RBRA",    "\]", "a closing square bracket")
T.COMMA =   Shape("COMMA",   ",", "a comma")
T.ID =      Shape("ID",      "[a-zA-Z_][a-zA-Z_0-9]*", "a label name")

asmTokShapes = [T.WS, T.COMMENT, T.REG, T.ALIAS, T.HASH, T.LDR, T.STR,
                T.MOV, T.ADD, T.SUB, T.CMP, T.BEQ, T.BLT, T.JMP, T.POP, T.PUSH, T.SMW,
                T.RMW, T.DEC, T.HEX, T.STRING, T.LBRA, T.RBRA, T.COMMA, T.ID]

asmLexer = Lexer(asmTokShapes, [T.WS])


class AsmParser(Parser):
    
    def __init__(self,string,Wlevel=ParserWarning.Print):
        super().__init__(asmLexer,string,Wlevel)
        self.labeldefs = {}
        self.labeluses = {}
        
        self.asm = ASMcore()

    def symb(self):
        tok = self.pop()
        if tok == T.ID:
            if tok.val in self.labeluses:
                self.labeluses[tok.val].add(tok)
            else:
                self.labeluses[tok.val] = set([tok])
            return Label(tok.val)
        elif tok == T.HEX:
            v = int(tok.val,base=16)
            if v >= 0 and v < 65536:
                return Word(v)
            else:
                raise ParserError(self, f"line {tok.line}, column {tok.column}: Number out of bounds")
        elif tok == T.DEC:
            v = int(tok.val)
            if v >= 0 and v < 65536:
                return Word(v)
            else:
                raise ParserError(self,f"line {tok.line}, column {tok.column}: Number out of bounds")
        else:
            raise ParserError(self, f"line {tok.line}, column {tok.column}: Expected a label name or a 16-bit word (decimal or hexa), found {tok.description}.")
    
    def array(self):
        self.match(T.LBRA)
        res = [self.symb()]
        while self.peek() == T.COMMA:
            self.pop()
            res.append(self.symb())
        self.match(T.RBRA)
        return res

    def string(self):
        return decode_string(self.match(T.STRING).val[1:-1])
    
    def smw(self,labs):
        self.match(T.SMW)
        switch = {
            T.LBRA :   lambda: self.array(),
            T.DEC :    lambda: self.symb(),
            T.HEX :    lambda: self.symb(),
            T.ID :     lambda: self.symb(),
            T.STRING : lambda: self.string(),
            "_" :      "Expected a number, a string, a label, or an array."
        }
        self.asm.smw(self.dispatch(switch,"_"),label=labs)

    def rmw(self,labs):
        self.match(T.RMW)
        x = int(self.match(T.DEC).val)
        self.asm.rmw(x,label=labs)

    def reg(self):
        r = self.match(T.REG)
        rn = r.val[1:]
        if rn not in ("0","1","2","3","4","5","6","7"):
            raise ParserError(self, f"line {r.line}, column {r.column}: Register index can only be a number 0-7. Got instead '{rn}'")
        return Reg(int(rn))        
    
    def ipush(self,labs):
        self.match(T.PUSH)
        self.asm.push(self.reg(),label=labs)

    def ipop(self,labs):
        self.match(T.POP)
        self.asm.pop(self.reg(),label=labs)

    def _ldrStrSuff(self):
        tok = self.peek()
        if tok == T.LBRA:
            self.match(T.LBRA)
            ry = self.reg()
            self.match(T.RBRA)
            return ry
        elif tok == T.REG:
            self.warn(f"Missing bracket around register {tok.val}", "Adding one for you")
            ry = self.reg()
            return ry
        else:
            if tok == T.HASH:
                self.warn(f"There should not be a hash (#) here", "I will pretend it does not exist")
                self.match(T.HASH)
            return self.symb()

    def _comma(self):
        if self.peek() == T.COMMA:
            self.match(T.COMMA)
        else:
            self.warn("Missing comma", "Adding one for you")
    
    def ldr(self,labs):
        self.match(T.LDR)
        r = self.reg()
        self._comma()
        z = self._ldrStrSuff()
        self.asm.ldr(r,z,label=labs)

    def str(self,labs):
        self.match(T.STR)
        r = self.reg()
        self._comma()
        z = self._ldrStrSuff()
        self.asm.str(r,z,label=labs)

    def _regopSuf(self):
        self._comma()
        tok = self.peek()
        if tok == T.REG:
            return self.reg()
        else:
            if tok == T.HASH:
                self.match(T.HASH)
                return self.symb()
            else:
                self.warn("Missing hash (#)", "Let me pretend it was there")
                return self.symb()
    
    def mov(self,labs):
        self.match(T.MOV)
        r = self.reg()
        self.asm.mov(r,self._regopSuf(),label=labs)

    def cmp(self,labs):
        self.match(T.CMP)
        r = self.reg()
        self.asm.cmp(r,self._regopSuf(),label=labs)

    def add(self,labs):
        self.match(T.ADD)
        rx = self.reg()
        self._comma()
        ry = self.reg()
        self.asm.add(rx,ry,self._regopSuf(),label=labs)

    def sub(self,labs):
        self.match(T.SUB)
        rx = self.reg()
        self._comma()
        ry = self.reg()
        self.asm.sub(rx,ry,self._regopSuf(),label=labs)

    def branch(self,shape,op,labs):
        self.match(shape)
        tok = self.peek()
        if tok == T.REG:
            op(self.reg())
        else:
            if tok == T.HASH:
                self.warn(f"Spurious hash (#)", "I will pretend I did not see it")
                self.match(T.HASH)
            op(self.symb(),label=labs)
    
    def inst(self):
        labelsToAdd = set()
        self.passComment()
        while self.peek() == T.ALIAS:
            tok = self.match(T.ALIAS)
            self.passComment()
            lab = tok.val[1:]
            if lab not in self.labeldefs:
                labelsToAdd.add(lab)
                self.labeldefs[lab] = tok
            elif lab in labelsToAdd:
                self.warn(f"{tok.val} defined twice in a row: on {tok.line}, col {tok.column} and line {self.labeldefs[lab].line}, col {self.labeldefs[lab].column}", "Skipping the second definition")
            else:
                raise ParserError(self, f"Duplicate labels : {tok.val} is defined on line {tok.line}, col {tok.column} but also at line {self.labeldefs[lab].line}, col {self.labeldefs[lab].column}.")
        switch = {
            T.RMW :  lambda: self.rmw(labelsToAdd),
            T.SMW :  lambda: self.smw(labelsToAdd),
            T.PUSH : lambda: self.ipush(labelsToAdd),
            T.POP :  lambda: self.ipop(labelsToAdd),
            T.STR :  lambda: self.str(labelsToAdd),
            T.LDR :  lambda: self.ldr(labelsToAdd),
            T.MOV :  lambda: self.mov(labelsToAdd),
            T.CMP :  lambda: self.cmp(labelsToAdd),
            T.ADD :  lambda: self.add(labelsToAdd),
            T.SUB :  lambda: self.sub(labelsToAdd),
            T.BEQ :  lambda: self.branch(T.BEQ,self.asm.beq,labelsToAdd),
            T.BLT :  lambda: self.branch(T.BLT,self.asm.blt,labelsToAdd),
            T.JMP :  lambda: self.branch(T.JMP,self.asm.b,labelsToAdd),
            "_":    "Expected a label definition or an instruction."
        }
        self.dispatch(switch, "_")

    def passComment(self):
        while self.peek() and self.peek() == T.COMMENT:
            self.match(T.COMMENT)
            
    def program(self):
        self.passComment()
        while not self.peek().isEOF():
            self.inst()
            self.passComment()


######################################################################
### ASMcore together with a file import tool
### 


class ASM(ASMcore):
    """Encapsulate the writing of ASM programs.

Once the ASM program is described, one can print it with 

 * show : the list of ASM instructions
 * showLst : the format "lst"
 * showMem : the format "mem", eatable by the logisim machine

All of them accept the optional argument "filename" to send the stream
to a file instead of stdout.

Typical use-case:

l1 = Label(prefix="blah")

c = ASM()

c.smw(12,label=l1)     # @blah smw 12
c.mov(Reg(0),2)        # mov r0, #2
c.mov(Reg(1),l1)       # mov r1, #blah
c.add(Reg(1),Reg(0),3) # add r1, r0, #3
c.ldr(Reg(3),Reg(1))   # ldr r3, [r1]
c.str(Reg(3),l1)       # str r3, #blah

c.showMem()

    """ 

    def load(self,filename):
        """read and parse filename into an AST object, then concat it at the
        end of self.

        """
        with open(filename, 'r') as file:
            source = file.read()
            parser = AsmParser(source)
            parser.program()
            self.concat(parser.asm)
    






######################################################################
### Self-contained command-line tool
### 

_help_text = """Usage: assembler.py [OPTIONS] [FILE.asm]
Turn FILE.asm file into FILE.mem and FILE.lst.
 - FILE.mem : list of 16-bit words, as used by the logisim processor
 - FILE.lst : mem file annotated with source code

Options:
  -h, --help           print this message
      --stdin          read from the standard input
      --wlevel=LEVEL   how warnings are dealt with. LEVEL can be error (default), warn or hide.
      --dump-asm       print on stdout a reconstruction of the ASM file.
"""

def _help():
    print(_help_text, file=sys.stderr)

def _main(argv):
    try:
        opts, args = getopt.getopt(argv,"h", ["help", "stdin", "wlevel=","dump-asm"])
    except getopt.GetoptError:
        _help()
        return 2
    source = None
    wlevel = "error"
    wval = {"error" : ParserWarning.Error,
             "warn" : ParserWarning.Print,
             "hide" : ParserWarning.Hide}
    dumpAsm = False
    for opt, arg in opts:
        if opt == '-h' or opt == '--help':
            _help()
            return 0
        elif opt == '--stdin':
            source = sys.stdin.read()
            filepref = "out"
        elif opt == '--wlevel':
            if arg in wval:
                wlevel = arg
            else:
                print(f"Argument '{arg}' invalid")
                _help()
                return 2
        elif opt == "--dump-asm":
            dumpAsm = True
    if not source:
        if len(args) != 1:
            _help()
            return 2
        filename = args[0]
        filepref = filename.split('.')[0]
        with open(filename, 'r') as file:
            source = file.read()
    try:
        parser = AsmParser(source, Wlevel=wval[wlevel])
        parser.program()
        parser.asm.showLst(filename=filepref + ".lst")
        print(f"Info: Wrote {filepref}.lst")
        parser.asm.showMem(filename=filepref + ".mem")
        print(f"Info: Wrote {filepref}.mem")
        if dumpAsm:
            print("\nReconstructed list of instructions, for the record:\n")
            parser.asm.show()
    except RecognitionError as e:
        print("Lexer Error: " + str(e))
        if e.lexer.idx >= len(e.lexer.string):
            raise Exception("This should not happen...")
        c = e.lexer.string[e.lexer.idx]
        s = ""
        if c == "'" or c == '"':
            print("It was trying to read a string: it is probably invalid.")
            print("The only ASCII codes allowed are 32 to 126, with escaped characters: '\\n', '\\t', '\\n', '\\'', '\\\"'.")
        else:
            if ord(c) > 127:
                print(f"Under the cursor is the non-ASCII character '{e.lexer.string[e.lexer.idx]}', of code {ord(e.lexer.string[e.lexer.idx])}")
            else:
                print(f"I suspect the character under the cursor is invalid: '{e.lexer.string[e.lexer.idx]}', of code {ord(e.lexer.string[e.lexer.idx])}")
            print("Could it be a problem of copy-and-paste?")
    except ParserWarning as e:
        print("Fatal warning: " + str(e), file = sys.stderr)
        print("Consider using '--wlevel=warn' to bypass.", file = sys.stderr)
    except ParserError as e:
        print("Parser Error: " + str(e), file = sys.stderr)
    except MissingLabelException as e:
        print("Assembler Error: " + str(e), file = sys.stderr)
        s = "The label is used on "
        for tok in parser.labeluses[e.label]:
            s += f"line {tok.line}, column {tok.column}, and "
        print(s[:-6] + ".")
    return 0
   
if __name__ == '__main__':
    try:
        sys.exit(_main(sys.argv[1:]))
    except FileNotFoundError as e:
        print(e)
    except PermissionError as e:
        print("Error: cannot write to output file")
        print(e)
